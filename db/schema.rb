# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141021212801) do

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "activities", force: true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type"
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type"
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type"

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true

  create_table "attribute_names", force: true do |t|
    t.string "name"
  end

  add_index "attribute_names", ["name"], name: "index_attribute_names_on_name", unique: true

  create_table "circles", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.integer  "level"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "circles", ["level"], name: "index_circles_on_level"
  add_index "circles", ["user_id"], name: "index_circles_on_user_id"

  create_table "collections", force: true do |t|
    t.string   "name",        limit: 64
    t.text     "description", limit: 600
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "collections", ["name", "created_at", "updated_at"], name: "index_collections_on_name_and_created_at_and_updated_at"
  add_index "collections", ["user_id"], name: "index_collections_on_user_id"

  create_table "comments", force: true do |t|
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "user_id"
    t.string   "message"
    t.integer  "rate"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["commentable_id", "commentable_type"], name: "index_comments_on_commentable_id_and_commentable_type"

  create_table "date_attributes", force: true do |t|
    t.integer  "user_id"
    t.integer  "item_id"
    t.integer  "attribute_name_id"
    t.datetime "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "date_attributes", ["attribute_name_id"], name: "index_date_attributes_on_attribute_name_id"
  add_index "date_attributes", ["item_id"], name: "index_date_attributes_on_item_id"
  add_index "date_attributes", ["user_id"], name: "index_date_attributes_on_user_id"

  create_table "decimal_attributes", force: true do |t|
    t.integer  "user_id"
    t.integer  "item_id"
    t.integer  "attribute_name_id"
    t.decimal  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "decimal_attributes", ["attribute_name_id"], name: "index_decimal_attributes_on_attribute_name_id"
  add_index "decimal_attributes", ["item_id"], name: "index_decimal_attributes_on_item_id"
  add_index "decimal_attributes", ["user_id"], name: "index_decimal_attributes_on_user_id"

  create_table "elements", force: true do |t|
    t.integer  "collection_id"
    t.integer  "item_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image"
    t.text     "description",   limit: 600
  end

  add_index "elements", ["collection_id"], name: "index_elements_on_collection_id"
  add_index "elements", ["item_id"], name: "index_elements_on_item_id"

  create_table "items", force: true do |t|
    t.string   "name",        limit: 64
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image"
    t.text     "description", limit: 600
    t.integer  "user_id"
  end

  create_table "posts", force: true do |t|
    t.datetime "publication"
    t.string   "title"
    t.text     "content",     limit: 10000
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  create_table "relationships", force: true do |t|
    t.integer  "following_user_id"
    t.integer  "followed_user_id"
    t.integer  "circle_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "relationships", ["followed_user_id"], name: "index_relationships_on_followed_user_id"
  add_index "relationships", ["following_user_id"], name: "index_relationships_on_following_user_id"

  create_table "reports", force: true do |t|
    t.integer  "user_id"
    t.string   "issue_type"
    t.string   "title"
    t.text     "content",    limit: 320
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "reports", ["user_id"], name: "index_reports_on_user_id"

  create_table "roles", force: true do |t|
    t.string   "name",       default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name"], name: "index_roles_on_name", unique: true

  create_table "social_permits", force: true do |t|
    t.string   "permissible_type"
    t.integer  "permissible_id"
    t.boolean  "denied",           default: false
    t.string   "admittable_type"
    t.integer  "admittable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "social_permits", ["admittable_id"], name: "index_social_permits_on_admittable_id"
  add_index "social_permits", ["permissible_id"], name: "index_social_permits_on_permissible_id"

  create_table "state_attributes", force: true do |t|
    t.integer  "user_id"
    t.integer  "item_id"
    t.integer  "attribute_name_id"
    t.boolean  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "state_attributes", ["attribute_name_id"], name: "index_state_attributes_on_attribute_name_id"
  add_index "state_attributes", ["item_id"], name: "index_state_attributes_on_item_id"
  add_index "state_attributes", ["user_id"], name: "index_state_attributes_on_user_id"

  create_table "string_attributes", force: true do |t|
    t.integer  "user_id"
    t.integer  "item_id"
    t.integer  "attribute_name_id"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "string_attributes", ["attribute_name_id"], name: "index_string_attributes_on_attribute_name_id"
  add_index "string_attributes", ["item_id"], name: "index_string_attributes_on_item_id"
  add_index "string_attributes", ["user_id"], name: "index_string_attributes_on_user_id"

  create_table "user_profiles", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.text     "description", limit: 1000
    t.string   "location"
    t.integer  "gender",      limit: 4,    default: 3
    t.string   "website"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar"
  end

  add_index "user_profiles", ["user_id"], name: "index_user_profiles_on_user_id"

  create_table "user_roles", force: true do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_roles", ["role_id"], name: "index_user_roles_on_role_id"
  add_index "user_roles", ["user_id"], name: "index_user_roles_on_user_id"

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username",               default: "", null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["username"], name: "index_users_on_username", unique: true

  create_table "votes", force: true do |t|
    t.integer  "user_id"
    t.integer  "votable_id"
    t.string   "votable_type"
    t.boolean  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["user_id"], name: "index_votes_on_user_id"
  add_index "votes", ["votable_id", "votable_type"], name: "index_votes_on_votable_id_and_votable_type"

end
