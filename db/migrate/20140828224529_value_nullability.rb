class ValueNullability < ActiveRecord::Migration
  def change
    change_column_null :date_attributes, :value, true
    change_column_null :decimal_attributes, :value, true
    change_column_null :state_attributes, :value, true
    change_column_null :string_attributes, :value, true
  end
end
