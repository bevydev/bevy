class ChangePostContentFieldType < ActiveRecord::Migration
  def change
    change_column :posts, :content, :text, limit: 10000
  end
end
