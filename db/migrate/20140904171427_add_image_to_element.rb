class AddImageToElement < ActiveRecord::Migration
  def change
    add_column :elements, :image, :string
  end
end
