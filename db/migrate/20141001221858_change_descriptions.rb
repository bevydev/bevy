class ChangeDescriptions < ActiveRecord::Migration
  def change
    change_column :user_profiles, :description, :text, limit: 1000
    change_column :collections, :description, :text, limit: 600
    change_column :elements, :description, :text, limit: 600
    change_column :items, :description, :text, limit: 600
  end
end
