class AddIndicesToCollection < ActiveRecord::Migration
  add_index :collections, [:name, :created_at, :updated_at]
end
