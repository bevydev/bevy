class CreateAttributeNames < ActiveRecord::Migration
  def change
    create_table :attribute_names do |t|
      t.string :name

      t.timestamps
    end
    add_index :attribute_names, :name, unique: true
  end
end
