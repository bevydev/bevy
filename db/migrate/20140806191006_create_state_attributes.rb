class CreateStateAttributes < ActiveRecord::Migration
  def change
    create_table :state_attributes do |t|
      t.references :user, index: true
      t.references :item, index: true
      t.references :attribute_name, index: true
      t.boolean :value

      t.timestamps
    end
  end
end
