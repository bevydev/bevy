class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.references :user, index: true
      t.string :issue_type
      t.string :title
      t.string :content
      t.timestamps
    end
  end
end
