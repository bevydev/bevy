class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.datetime :publication
      t.string :title
      t.string :content

      t.timestamps
    end

    add_reference :posts, :user


  end
end
