class ChangeItemDescriptionFieldType < ActiveRecord::Migration
  def change
    change_column :items, :description, :text, limit: 320
  end
end
