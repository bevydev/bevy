class CreateDecimalAttributes < ActiveRecord::Migration
  def change
    create_table :decimal_attributes do |t|
      t.references :user, index: true
      t.references :item, index: true
      t.references :attribute_name, index: true
      t.decimal :value

      t.timestamps
    end
  end
end
