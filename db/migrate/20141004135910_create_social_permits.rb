class CreateSocialPermits < ActiveRecord::Migration
  def change
    create_table :social_permits do |t|
      t.string :permissible_type
      t.integer :permissible_id
      t.boolean :denied, default: false
      t.string :admittable_type
      t.integer :admittable_id

      t.timestamps
    end
    add_index :social_permits, :permissible_id
    add_index :social_permits, :admittable_id
  end
end
