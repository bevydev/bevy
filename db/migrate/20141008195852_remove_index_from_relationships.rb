class RemoveIndexFromRelationships < ActiveRecord::Migration
  def change
    remove_index :relationships, [:following_user_id, :followed_user_id]
  end
end
