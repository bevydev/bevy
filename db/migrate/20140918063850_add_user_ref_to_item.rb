class AddUserRefToItem < ActiveRecord::Migration
  def change
    add_reference :items, :user, index: false
  end
end
