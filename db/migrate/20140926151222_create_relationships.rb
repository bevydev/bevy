class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :following_user_id
      t.integer :followed_user_id
      t.integer :circle_id

      t.timestamps
    end

    add_index :relationships, :following_user_id
    add_index :relationships, :followed_user_id
    add_index :relationships, [:following_user_id, :followed_user_id], unique: true
  end
end
