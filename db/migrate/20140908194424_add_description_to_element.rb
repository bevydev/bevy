class AddDescriptionToElement < ActiveRecord::Migration
  def change
    add_column :elements, :description, :string
  end
end
