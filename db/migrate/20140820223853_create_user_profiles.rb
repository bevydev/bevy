class CreateUserProfiles < ActiveRecord::Migration
  def change
    create_table :user_profiles do |t|
      t.references :user, index: true
      t.string :name
      t.string :description
      t.string :location
      t.integer :gender, limit: 4, precision: 1, default: 3
      t.string :website
      t.timestamps
    end
  end
end
