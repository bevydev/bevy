class ChangeReportContentFieldType < ActiveRecord::Migration
  def change
    change_column :reports, :content, :text, limit: 320
  end
end

