class ChangeElementDescriptionFieldType < ActiveRecord::Migration
  def change
    change_column :elements, :description, :text, limit: 320
  end
end
