class CreateStringAttributes < ActiveRecord::Migration
  def change
    create_table :string_attributes do |t|
      t.references :user, index: true
      t.references :item, index: true
      t.references :attribute_name, index: true
      t.string :value

      t.timestamps
    end
  end
end
