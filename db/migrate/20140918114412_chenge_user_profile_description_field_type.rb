class ChengeUserProfileDescriptionFieldType < ActiveRecord::Migration
  def change
    change_column :user_profiles, :description, :text, limit: 500
  end
end
