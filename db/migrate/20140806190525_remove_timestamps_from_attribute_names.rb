class RemoveTimestampsFromAttributeNames < ActiveRecord::Migration
  def change
    change_table :attribute_names do
      remove_column :attribute_names, :created_at, :string
      remove_column :attribute_names, :updated_at, :string
    end
  end
end
