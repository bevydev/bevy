class AddLengthConstraints < ActiveRecord::Migration
  def change
    change_column :collections, :description, :text, limit: 500
    change_column :collections, :name, :string, limit: 64
    change_column :items, :name, :string, limit: 64
  end
end
