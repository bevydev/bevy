class CreateDateAttributes < ActiveRecord::Migration
  def change
    create_table :date_attributes do |t|
      t.references :user, index: true
      t.references :item, index: true
      t.references :attribute_name, index: true
      t.datetime :value

      t.timestamps
    end
  end
end
