class CreateElements < ActiveRecord::Migration
  def change
    create_table :elements do |t|
      t.references :collection, index: true
      t.references :item, index: true

      t.timestamps
    end
  end
end
