class CreateCircles < ActiveRecord::Migration
  def change
    create_table :circles do |t|
      t.integer :user_id
      t.string :name
      t.integer :level

      t.timestamps
    end

    add_index :circles, :user_id
    add_index :circles, :level
  end
end
