namespace :bevy do
  namespace :relationships do
    desc "Update old db users with public circles"
    task :makecircles => :environment do
      User.all.each(&:make_public_circle!)
    end
  end
end