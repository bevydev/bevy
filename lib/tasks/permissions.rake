namespace :bevy do
  namespace :permissions do
    require "#{Rails.root}/lib/modules/permissions.rb"

    desc "Generate new permissions.yml file"
    task :generate => :environment do

      File.open "#{Rails.root}/config/permissions.yml", "w" do |f|
        f.write <<COMMENT
# This is file containing permissions for all controllers and actions.
# There are two parts: one defining user groups and one for defining permissions.
#
# Role groups definition follows this schema:
# role_groups:
#   <role_group_name1>:
#   - <role_name1>
#   - <role_name2>
#   - ...
#
# This part is optional and can be omitted if you don't want to define role groups.
#
# Permissions definitions are defined this way:
# <controller_path>:
#   [default:
#     - <role1>
#     - <role2>]
#   <action1>:
#   - <role1>
#   - <role2>
#
# <controller_path> variable needs to be defined in routes (for available routes try: rake routes)
# <actions> are names of methods defined in controllers (also visible in rake routes)
# <role> is either role_group defined in role_groups or role from Roles table
# special roles are:
#   - all => anybody has permission to perform this action
#   - logged_in => only logged in users are allowed to perform this action
# Defining these roles in Roles table is not recommended
# If no permission level is defined, it defaults to logged_in

COMMENT
        f.write Permissions::PermissionManager.generate_yaml
      end
    end

    desc "Append default permissions for selected controller"
    task :append, [:controller] => :environment do |t, args|
      args.with_defaults controller: nil

      File.open "#{Rails.root}/config/permissions.yml", "a" do |f|
        f.write Permissions::PermissionManager.generate_yaml args.controller
      end
    end
  end
end
