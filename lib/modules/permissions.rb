module Permissions
  require 'yaml'

  class PermissionManager
    # Generates yaml code consisting information about all actions that has routes to them
    # Default value for each action is "all", meaning that all users, even not signed in,
    # is allowed to perform do this action
    def self.generate_yaml(controller = nil)
      result = {}

      Rails.application.routes.routes.to_a.each do |rota|
        if rota.app.instance_variable_defined? '@defaults'
          path = rota.defaults

          if controller
            next unless path[:controller].downcase.include? controller.downcase
          end

          result[path[:controller]] = {} if result[path[:controller]].nil?
          result[path[:controller]][path[:action]] = ["all"]
        end
      end

      result.to_yaml
    end

    def self.permitted?(controller, current_user)
      r = roles(controller, controller.action_name)

      return true if r.include? "all"

      if current_user
        return true if r.include? "logged_in"
        user_roles = current_user.roles
        r.any? { |role | user_roles.any? { |permitted| permitted.name == role } }
      else
        false
      end
    end

    # Checks if <tt>user</tt> has <tt>role</tt>
    # <tt>role</tt> can be a role group defined in <tt>permissions.yml</tt> or just a string with explicit <tt>role</tt> name,
    # as it is in the database. Method has_role? will consider role first as group and
    # if group with such name exists, will check if <tt>user</tt> has any of the roles found in the group.
    # Otherwise, if there is no group with <tt>role</tt> name, user's roles will be checked against those in the database
    def self.has_role?(user, role)
      return true if (role == "all") || (role == "logged_in" && user)
      return false if user.nil?

      perm = load_permissions
      role = perm["role_groups"].nil? ? [role] : (perm["role_groups"][role] || [role])

      user_roles = user.roles
      role.any? { |role | user_roles.any? { |permitted| permitted.name == role } }
    end

    private
    def self.roles(controller, action)
      perm = load_permissions
      list = perm[controller.controller_path]

      if list.nil?
        result = ["logged_in"] # defaults to logged_in permission
      elsif list[action].nil?
        result = list["default"].nil? ? ["logged_in"] : list["default"]
      else
        result = list[action] || ["logged_in"]
      end

      perm["role_groups"].nil? ? result : result.map { |role| perm["role_groups"][role] || role }.flatten
    end

    def self.load_permissions
      YAML.load_file("#{Rails.root}/config/permissions.yml") || {}
    end
  end
end