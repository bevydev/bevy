# attributes that belong tu special user are considered to be default attributes
SPECIAL_USER_ID = 1

ATTRIBUTE_TYPES = ["date", "decimal", "state", "string"]