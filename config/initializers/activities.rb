module PublicActivity
  class Activity

    def profile_activity?(user = current_user, shown_user)
      (self.trackable && self.get_owner) ? self.trackable.activity_visible_to?(user) && shown_user == self.get_owner : false
    end

    def user_activity?(user = current_user)
      (self.trackable && self.get_owner) ? self.trackable.activity_visible_to?(user) && user != self.get_owner && user.is_following?(self.get_owner) : false
    end

    def get_owner
      User.find_by_id(self.owner_id)
    end
  end
end 