Rails.application.routes.draw do

  get 'errors/not_found'

  get 'errors/unacceptable'

  get 'errors/internal_error'

  get 'errors/under_construction'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  resources :activities
  resources :reports
  resources :relationships, only: [:create, :destroy]

  concern :permissible do
    post :public_share
    post :public_unshare
  end


  root 'static_pages#home'

  get 'static_pages/help'
  get 'static_pages/home'

  get "/404", :to => "errors#not_found"
  get "/422", :to => "errors#unacceptable"
  get "/500", :to => "errors#internal_error"
  get "/underconst", :to => "errors#under_construction"

  resources :collections do
    collection do
      get :autocomplete
      get :name_available
    end
    resources :elements
    member do
      concerns :permissible
      get :load_slideshow
    end
  end

  resources :items do
    collection do
      get :autocomplete
    end
    root to: 'items#index'
  end

  resources :posts

  get 'search/results'

  devise_for :users,
             :controllers => {
                 :registrations => 'registrations',
                 :user_profiles => 'user_profiles'
             }

  devise_scope :user do
    get :username_available, controller: "registrations"
    get :email_available, controller: "registrations"
  end

  resources :user_profiles do
    collection do
      get :autocomplete
    end
    member do
      get '/following', to: "relationships#index"
      # get '/followers', to: "circles#index"
      get '/social', to: "social#index"
      get "/following_list", to: "social#following_list", as: "following_list"
    end
    resources :circles, path: "/followers"
    get "/set_users", to: "circles#set_users", as: "set_users"
  end

  concern :votable do
    member do
      put :upvote
      put :downvote
      put :unvote
    end
  end

  concern :commentable do
    resources :comments, concerns: :votable
  end

  resources :comments, concerns: :commentable, shallow: true

  resources :items, concerns: :commentable, shallow: true

  resources :collections, shallow: true do
    resources :elements, concerns: :commentable
  end

  mount Markitup::Rails::Engine, at: "markitup", as: "markitup"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
