ActiveAdmin.register User do
  permit_params :username, :email, :password

  index do
    selectable_column
    id_column
    column :username
    column :email
    column :current_sign_in_at
    column :current_sign_in_ip
    column :created_at
    actions do |record|
        link_to "Add Role", new_admin_user_role_path(:user_role => { :user_id => record })
  end
  end

  form do |f|
    f.inputs "User Details" do
      f.input :username
      f.input :email
      f.input :password
    end
    f.actions
  end

  filter :roles
  filter :username
  filter :email
  filter :created_at
  filter :updated_at
  filter :current_sign_in_at
  filter :current_sign_in_ip
  filter :last_sign_in_at
  filter :last_sign_in_ip

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
