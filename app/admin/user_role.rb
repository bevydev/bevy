ActiveAdmin.register UserRole do

  permit_params :user_id, :role_id

  index do
    selectable_column
    id_column
    column "User" do |record|
      user = User.find(record.user_id)
      link_to user.username, [:admin, user]
    end
    column "Role" do |record|
      role = Role.find(record.role_id)
      link_to role.name, [:admin, role]
    end
    column :created_at
    column :updated_at
    actions
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
