ActiveAdmin.register Post do
  permit_params :user_id, :publication, :title, :content

  index do
    selectable_column
    id_column
    column "User" do |record|
      user = User.find(record.user_id)
      link_to user.username, [:admin, user]
    end
    column :title
    column :publication
    column :created_at
    column :updated_at
    actions
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
