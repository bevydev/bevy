ActiveAdmin.register Report do
  permit_params :user_id, :issue_type, :title, :content

  index do
    selectable_column
    id_column
    column :issue_type
    column "User" do |record|
      user = User.find(record.user_id)
      link_to user.username, [:admin, user]
    end
    column :title
    column :created_at
    column :updated_at
    actions
  end

  form do |f|
    f.inputs "Report Details" do
      f.input :user_id, :as => :select, :collection => User.all.map{|u| ["#{u.username}", u.id]}
      f.input :issue_type, :as => :select, :collection => ["Problem", "Idea", "Opinion"]
      f.input :title
      f.input :content
    end
    f.actions
  end

  filter :user
  filter :issue_type, :as => :select, :collection => ["Problem", "Idea", "Opinion"]
  filter :title
  filter :content
  filter :created_at
  filter :updated_at

end
