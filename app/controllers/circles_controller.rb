class CirclesController < ApplicationController
  before_action :set_profile
  before_action :check_user_existence, except: [:set_users]
  before_action :set_circle, only: [:show, :edit, :update, :destroy, :set_users]

  def index
    @circles = @shown_user.circles
  end

  def show
    @circles = @shown_user.circles
    redirect_to user_profile_circles_path, alert: "Requested circle not found" and return unless @circle
  end

  def set_users
    @circles = @shown_user.circles
    followers_ids = Relationship.where(followed_user_id: params[:user_profile_id], circle_id: params[:id]).pluck("following_user_id")
    @followers = User.where(id: followers_ids)
    respond_to do |format|
      format.js
    end
  end

  def new
    @circle = Circle.new
  end

  def edit
    redirect_to user_profile_circles_path, alert: "Requested circle not found" and return unless @circle
  end

  def create
    @circle = Circle.new(circle_params)
    @circle.user_id = current_user.id
    @circle.level = 1

    respond_to do |format|
      if @circle.save
        if params[:following_user_id]
          Relationship.create(followed_user_id: current_user.id, following_user_id: params[:following_user_id], circle_id: @circle.id)
        end
        format.html { redirect_to user_profile_circles_path, notice: 'Circle was successfully created.' }
        format.json { render action: 'show', status: :created, location: @circle }
      else
        format.html { render action: 'new' }
        format.json { render json: @circle.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @circle.update(circle_params)
        format.html { redirect_to user_profile_circle_path(@shown_user, @circle), notice: 'Circle was successfully updated.' }
        format.json { render action: 'show', status: :ok, location: @circle }
      else
        format.html { render action: 'edit' }
        format.json { render json: @circle.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @circle.destroy
    respond_to do |format|
      format.html { redirect_to user_profile_circles_path @shown_user }
      format.json { head :no_content }
    end
  end

  private
  def set_circle
    begin
      @circle = Circle.find(params[:id])
      redirect_to(user_profile_circles_path, alert: "Access forbidden", status: :forbidden) \
        unless @circle.user_id == current_user.id
    rescue
      @circle = nil
    end
  end

  def circle_params
    params[:circle].permit(:name)
  end

  def set_profile
    @shown_user = User.find(params[:user_profile_id])
    @shown_user_profile = @shown_user.user_profile
  end

  def check_user_existence
    redirect_to current_user.user_profile unless User.exists?(params[:user_profile_id])
  end
end
