class ApplicationController < ActionController::Base
  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_filter { |controller| check_permission controller }
  before_action :set_user_profile
  before_action :set_collections

  include ApplicationHelper
  include PublicActivity::StoreController

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :username
    devise_parameter_sanitizer.for(:account_update) << :username
  end

  private
  def set_user_profile
    begin
      @user_profile = UserProfile.where(user_id: current_user.id).select(:id, :name, :avatar).first
    rescue
      @user_profile = nil
    end
  end

  def set_collections
    begin
      @user_collections = Collection.where(user_id: current_user.id).order(updated_at: :desc).select(:id, :name).limit(5)
    rescue
      @user_collections = nil
    end
  end

  def check_permission(controller)
    unless Permissions::PermissionManager.permitted? controller, current_user
      if current_user
        redirect_to static_pages_home_path, alert: "Access forbidden"
      else
        authenticate_user!
      end
    end
  end
end
