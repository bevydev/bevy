class RelationshipsController < ApplicationController
  before_action :set_profile, only: [:index]
  before_action :check_user_existence, only: [:index]

  def index
    @followers = @shown_user.followed_users
  end

  def create
    @user = User.find(params[:relationship][:followed_user_id])
    if params[:relationship][:circle_id]
      @following_user = User.find(params[:relationship][:following_user_id])
      @circles = @user.circles
      circle = Circle.find(params[:relationship][:circle_id])
      @user.add_to_circle!(@following_user, circle)
    else
      current_user.follow!(@user)
    end
    respond_to do |format|
      format.html { redirect_to @user.user_profile}
      format.js
    end
  end

  def destroy
    relationship = Relationship.find(params[:id])
    @user = relationship.followed_user
    if params[:relationship]
      @following_user = User.find(params[:relationship][:following_user_id])
      @circles = @user.circles
      relationship.destroy
    else
      current_user.unfollow!(@user)
    end
    respond_to do |format|
      format.html { redirect_to @user.user_profile}
      format.js
    end
  end


  private

  def check_user_existence
    redirect_to current_user.user_profile unless User.exists?(params[:id])
  end

  def set_profile
    @shown_user = User.find(params[:id])
    @shown_user_profile = @shown_user.user_profile
  end


end
