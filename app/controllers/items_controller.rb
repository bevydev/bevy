class ItemsController < ApplicationController
  include CommentableController

  # before_action :authenticate_user!, except: [:index, :show]
  before_action :set_item, only: [:show, :edit, :update, :destroy]
  # before_action :set_commentable, only: [:show]

  # GET /items
  # GET /items.json
  def index
    if params[:query].present?
      @items = Item.search params[:query].downcase
    else
      @items = Item.all
    end
  
    @attributes = []
  end

  def autocomplete
    render json: Item.search_item_auto(params[:term].downcase)
  end

  # GET /items/1
  # GET /items/1.json
  def show
    if signed_in?
      @available_cols = Collection.find_available_collections(current_user.id, @item)
    else
      @available_cols = []
    end

    if @available_cols.empty?
      @collection = nil and return
    end

    if cookies[:collection_redirect_cookie]
      begin
        @collection = Collection.find(cookies[:collection_redirect_cookie])
        @collection = @available_cols.first unless @available_cols.include? @collection
      rescue
        @collection = @available_cols.first
      end
    else
      @collection = @available_cols.first
    end
    @attributes = @item.all_attributes

    @ago = (Date.today - @item.created_at.to_date).to_i
  end

  # GET /items/new
  def new
    @item = Item.new
    @attributes = []
  end

  # GET /items/1/edit
  def edit
    # redirect_to(item_path, alert: "Access forbidden") unless user_has_role?("Moderator")
    @attributes = @item.all_attributes
  end

  # POST /items
  # POST /items.json
  def create
    @item = Item.new(item_params)
    @item.user_id = current_user.id

    saved = @item.save
    @attributes =  saved ? ItemAttribute.parse(SPECIAL_USER_ID, @item.id, params[:item]) : []

    respond_to do |format|
      if saved && ItemAttribute.update(SPECIAL_USER_ID, @item.id, @attributes)
        format.html { redirect_to @item, notice: 'Item was successfully created.' }
        format.json { render action: 'show', status: :created, location: @item }
      else
        format.html { render action: 'new' }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    update_image

    saved = @item.update(item_params)
    @attributes = saved ? ItemAttribute.parse(SPECIAL_USER_ID, @item.id, params[:item]) : []

    respond_to do |format|
      if saved && ItemAttribute.update(SPECIAL_USER_ID, @item.id, @attributes)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { render action: 'show', status: :ok, location: @item }
      else
        format.html { render action: 'edit' }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.elements.destroy_all
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url }
      format.json { head :no_content }
    end
  end

  def set_commentable
    @commentable = Item.find_by_id(params[:id])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      begin
        @item = Item.find(params[:id])
      rescue
        redirect_to items_path, alert: "Item with id #{params[:id]} not found"
      end
    end

    def get_collection
      begin
        @collection = Collection.find(params[:collection_id])
        redirect_to(collections_path, alert: "Access forbidden") unless @collection.user_id == current_user.id
      rescue
        @collection = nil
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:name, :description, :image, :remove_image, :remote_image_url, :image_cache)
    end

    def update_image
      unless item_params[:image].blank? && item_params[:remote_image_url].blank?
        @item.remove_image!
      end

      unless @item.save
        redirect_to @item, alert: "Unable to update image."
      end
    end
end
