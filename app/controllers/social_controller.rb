class SocialController < ApplicationController
  before_action :set_profile
  before_action :check_user_existence

  def index
    if params[:query].present?
    @users = User.search params[:query].downcase
  else
    @users = User.all
  end
    @circles = @shown_user.circles
  end

  def following_list
    @followed = @shown_user.followed_users
    respond_to do |format|
      format.js
    end
  end

  private

  def set_profile
    @shown_user = User.find(params[:id])
    @shown_user_profile = @shown_user.user_profile
  end

  def check_user_existence
    redirect_to current_user.user_profile unless User.exists?(params[:id])
  end
end