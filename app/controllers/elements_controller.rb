class ElementsController < ApplicationController
  include CommentableController

  before_action :get_collection
  before_action :set_element, only: [:show, :edit, :update, :destroy]
  before_action :set_item, only: [:show, :edit, :update]
  before_action :set_attributes, only: [:show, :edit, :update, :destroy]
  before_action :check_owner, only: [:edit, :update, :destroy]
  # before_action :authenticate_user!

  # GET /items/1
  # GET /items/1.json
  def show
    unless @collection.elements.include? @element
      flash[:error] = "Requested item not found"
      redirect_to collection_path(@collection)
    end

    @ago = (Date.today - @element.item.created_at.to_date).to_i
  end

  # GET /items/new
  def new
    if params[:item_id]
      @item = Item.find(params[:item_id])
      if @collection.items.include? @item
        element = Element.where(collection_id: @collection.id, item_id: @item.id).first
        redirect_to collection_element_path(@collection, element) and return
      end
      @element = Element.new item: @item, collection: @collection
      @attributes = @element.item_attributes
    else
      cookies[:collection_redirect_cookie] = params[:collection_id]
      redirect_to items_path(:collection_id => params[:collection_id])
    end
  end

  # GET /items/1/edit
  def edit
  end

  # POST /items
  # POST /items.json
  def create
    item = Item.find(params[:item_id])

    @element = Element.new(element_params)
    @element.link(@collection, item)

    if element_params[:description] == item.description
      @element.description = nil
    end

    saved = @element.save
    @collection.touch if saved
    @attributes = saved ? ItemAttribute.parse(current_user.id, item.id, params[:element]) : []

    respond_to do |format|
      if saved && ItemAttribute.update(current_user.id, item.id, @attributes)
        format.html { redirect_to [@collection, @element], notice: 'Item was successfully created.' }
        format.json { render action: 'show', status: :created, location: @element }
      else
        format.html { render action: 'new' }
        format.json { render json: @element.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    update_image

    saved = @element.update(element_params)
    @attributes = saved ? ItemAttribute.parse(current_user.id, @element.item_id, params[:element]) : []

    if saved && ItemAttribute.update(current_user.id, @element.item_id, @attributes)
      @collection.touch
      redirect_to [@collection, @element], notice: 'Item was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @element.destroy
    @collection.touch
    respond_to do |format|
      format.html { redirect_to collection_path(@collection) }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_element
    begin
      @element = Element.find(params[:id])

      unless @collection.user_id == current_user.id || @collection.visible_to?(current_user)
        redirect_to(root_path)
      end
    rescue
      redirect_to @collection
    end
  end

  def set_item
    @item = Item.find(@element.item_id) unless @element.nil?
  end

  def set_commentable
    @commentable = Element.find_by_id(params[:id])
  end

  def get_collection
    begin
      unless Collection.exists?(params[:collection_id])
        redirect_to collections_path
      end

      @collection = Collection.find(params[:collection_id])

      unless @collection.user_id == current_user.id || @collection.visible_to?(current_user)
        redirect_to(collections_path, alert: "Access forbidden", status: :forbidden)
      end
    rescue
      @collection = nil
    end
  end

  def check_owner
    unless @collection.user_id == current_user.id
      redirect_to(collections_path, alert: "Access forbidden", status: :forbidden)
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def element_params
    params[:element].permit(:item_id, :description, :image, :remove_image, :remote_image_url, :image_cache, :attributes)
  end

  def set_attributes
    @attributes = @element.item_attributes if @element
  end

  def update_image
    unless element_params[:image].blank? && element_params[:remote_image_url].blank?
      @element.remove_image!
    end

    unless @element.save
      redirect_to @element, alert: "Unable to update image."
    end
  end
end
