class RegistrationsController < Devise::RegistrationsController

  def after_sign_up_path_for(resource)
    posts_path
  end

  def after_update_path_for(resource)
    edit_user_profile_path(resource)
  end

  def create
    super
    if resource.save
      @user_profile = UserProfile.new
      @user_profile.user_id = resource.id
      @user_profile.save
    end
  end

  def destroy
    super
    if resource.destroy
      @user_profile = UserProfile.find_by_user_id(resource.id)
      @user_profile.destroy
    end
  end

  def username_available
    valid = params[:username] == params[:current_username]
    valid ||= User.username_available?(params[:username])
    render json: valid
  end

  def email_available
    valid = params[:email] == params[:current_email]
    valid ||= User.email_available?(params[:email])
    render json: valid
  end

end