class CollectionsController < ApplicationController
  include PermissibleController

  before_action :set_collection, only: [:show, :load_slideshow]
  before_action :set_user_and_collection, only: [:edit, :update, :destroy]
  before_action :set_elements, only: [:show]
  # before_action :authenticate_user!

  helper_method :sort_param, :direction_param

  # GET /collections
  # GET /collections.json
  def index
    if params[:add_item]
      @item = Item.find(params[:add_item])
      @collections = Collection.find_available_collections(current_user.id, @item)
    elsif params[:query].present?
      @collections = Collection.search params[:query].downcase, where: {user: current_user.id}
    else
      @collections = current_user.collections
    end
  end

  def autocomplete
    render json: Collection.search_collection_auto(params[:term].downcase, current_user.id)
  end

  # GET /collections/1
  # GET /collections/1.json
  def show
    redirect_to collections_path, alert: "Requested collection not found" and return unless @collection
  end

  # GET /collections/new
  def new
    @collection = Collection.new
  end

  # GET /collections/1/edit
  def edit
  end

  # POST /collections
  # POST /collections.json
  def create
    @collection = Collection.new(collection_params)
    @collection.user_id = current_user.id

    respond_to do |format|
      if @collection.save
       if params["item_id"]
          format.html { redirect_to new_collection_element_path(@collection.id, :item_id => params["item_id"]), notice: 'Collection was successfully created.' }
          #format.json { render action: 'show', status: :created, location: new_collection_element_path(@collection.id, :item_id => params["item_id"]) }
        else
          format.html { redirect_to @collection, notice: 'Collection was successfully created.' }
          format.json { render action: 'show', status: :created, location: @collection }
        end
      else
        format.html { render action: 'new' }
        format.json { render json: @collection.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /collections/1
  # PATCH/PUT /collections/1.json
  def update
    respond_to do |format|
      if @collection.update(collection_params)
        format.html { redirect_to @collection, notice: 'Collection was successfully updated.' }
        format.json { render action: 'show', status: :ok, location: @collection }
      else
        format.html { render action: 'edit' }
        format.json { render json: @collection.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /collections/1
  # DELETE /collections/1.json
  def destroy
    @collection.elements.destroy_all
    @collection.destroy

    respond_to do |format|
      format.html { redirect_to collections_url, notice: 'Collection successfully destroyed' }
      format.json { head :no_content }
    end
  end

  def load_slideshow
    respond_to do |format|
      format.html { redirect_to @collection }
      format.js { render json: @collection.slideshow }
    end
  end

  def name_available
    valid = params[:name] == params[:current_name]
    valid ||= Collection.name_available?(params[:name], params[:user_id])
    render json: valid
  end

  private
    # Use callbacks to share common setup or constraints between actions.

  def sort_param
    Collection.column_names.include?(params[:sort]) ? params[:sort].to_sym : :name
  end

  def direction_param
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def set_collection
    begin
      @collection = Collection.find(params[:id])
      unless @collection.user_id == current_user.id || @collection.visible_to?(current_user)
        redirect_to(collections_path, alert: "Access forbidden")
      end
    rescue
      @collection = nil
    end
  end

  def set_user_and_collection
    begin
      @collection = Collection.find(params[:id])
      unless @collection.user_id == current_user.id
        redirect_to(collections_path, alert: "Access forbidden")
      end
    rescue
      @collection = nil
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def collection_params
    params[:collection].permit(:name, :current_name, :description, :add_item, :user_id)
  end

  def set_elements
    @elements = @collection.find_element_item_pairs unless @collection.nil?
  end

  def set_permissible
    @permissible = Collection.find(params[:id])
  end
end
