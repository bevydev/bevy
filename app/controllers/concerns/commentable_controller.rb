module CommentableController
  extend ActiveSupport::Concern

  included do
    before_action :set_commentable, only: [:show]
    before_action :set_comments, only: [:show]
  end

  def set_comments
    unless @commentable.nil?
      @comments = @commentable.comments_ordered
      @comment = @commentable.comments.new
      @comment.user_id = current_user.id if current_user
    end
  end

  def set_commentable
    raise NotImplementedError
  end
end