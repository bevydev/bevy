module VotableController
  extend ActiveSupport::Concern

  included do
    before_action :set_votable, only: [:upvote, :downvote, :unvote]
  end

  def upvote
    @votable.upvote_by(current_user)

    respond_to do |format|
      format.html { redirect_to @votable }
      format.js { render file: 'votes/upvote.js.erb' }
    end
  end

  def downvote
    @votable.downvote_by(current_user)

    respond_to do |format|
      format.html { redirect_to @votable }
      format.js { render file: 'votes/downvote.js.erb' }
    end
  end

  def unvote
    @votable.unvote_by(current_user)

    respond_to do |format|
      format.html { redirect_to @votable }
      format.js { render file: 'votes/unvote.js.erb' }
    end
  end

  private

  def set_votable
    raise NotImplementedError
  end
end