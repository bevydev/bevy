module PermissibleController
  extend ActiveSupport::Concern

  included do
    before_action :set_permissible, only: [:public_share, :public_unshare]
  end

  def public_share
    @permissible.permit_publicly
    respond_to do |format|
      format.html { redirect_to @permissible}
      format.js
    end
  end

  def public_unshare
    @permissible.prohibit_publicly
    respond_to do |format|
      format.html { redirect_to @permissible}
      format.js
    end
  end

  def set_permissible
    raise NotImplementedError
  end
end