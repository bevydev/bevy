class ActivitiesController < ApplicationController

  def index
    if user_signed_in?
      @activities = PublicActivity::Activity.order("created_at desc")
      @activities = @activities.select{|a| a.user_activity?(current_user)}
    end
  end
end
