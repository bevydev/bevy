class UserProfilesController < ApplicationController
  before_action :set_user_profile, only: [:index, :show, :edit, :update, :destroy]
  # before_action :check_user_existence
  # before_action :authenticate_user!

  def index
    if params[:query].present?
      @users = User.search params[:query].downcase
    else
      @users = User.all
    end
  end

  def autocomplete
    render json: User.search_user_auto(params[:term].downcase)
  end

  def show
    @shown_user = User.find(params[:id])
    redirect_to(user_profile_path, alert: "Access forbidden", status: :forbidden) unless @shown_user
    @shown_user_profile = @shown_user.user_profile
    @collections = @shown_user.collections.select { |c| c.visible_to?(current_user) }
    @activities = PublicActivity::Activity.order("created_at desc")
    @activities = @activities.select{|a| a.profile_activity?(current_user,@shown_user) }
    
  end

  def edit
  end

  def update
    update_avatar

    respond_to do |format|
      if @user_profile.update(user_profile_params)
        format.html { redirect_to @user_profile, notice: 'Profile was successfully updated.' }
        format.json { render action: 'show', status: :ok, location: @user_profile }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def user_profile_params
    params[:user_profile].permit(:name, :description, :location, :gender, :website, :avatar, :remove_avatar, :remote_avatar_url, :avatar_cache)
  end

  def set_user_profile
    begin
      @user_profile = UserProfile.find_by user_id: current_user.id
      unless @user_profile.user_id == current_user.id
        redirect_to(user_profile_path, alert: "Access forbidden", status: :forbidden)
      end
    rescue
      @user_profile = nil
    end
  end

  def check_user_existence
    redirect_to current_user.user_profile unless User.exists?(params[:id])
  end

  def update_avatar
    unless user_profile_params[:avatar].blank? && user_profile_params[:remote_avatar_url].blank?
      @user_profile.remove_avatar!
    end

    unless @user_profile.save
      redirect_to @user_profile, alert: "Unable to update avatar."
    end
  end
end
