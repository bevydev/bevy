class CommentsController < ApplicationController
  include VotableController

  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  before_action :set_commentable, only: [:index, :new, :create, :update, :destroy]

  # GET /comments
  # GET /comments.json
  def index
    unless @commentable
      @comments = Comment.all
    else
      @comments = @commentable.comments
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
  end

  # GET /comments/new
  def new
    @comment = @commentable.comments.new
  end

  # GET /comments/1/edit
  def edit
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = @commentable.comments.build(comment_params)
    @comment.user_id = current_user.id

    respond_to do |format|
      if @comment.save
        format.html { redirect_to comment_redirection, notice: 'Comment was successfully created.' }
        format.json { render action: 'show', status: :created, location: @comment }
        format.js
      else
        format.html { redirect_to comment_redirection, alert: 'Cannot save comment.' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to comment_redirection, notice: 'Comment was successfully updated.' }
        format.json { render action: 'show', status: :ok, location: @comment }
      else
        format.html { render action: 'edit' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to comment_redirection }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_comment
    begin
      @comment = Comment.find(params[:id])
    rescue
      redirect_to root_path, alert: 'Comment not found'
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def comment_params
    params[:comment].permit(:user_id, :commentable_id, :commentable_type, :message, :rate)
  end

  # Find nested comment's parent
  def set_commentable
    params.each do |key, value|
      if key =~ /(.+)_id$/
        @commentable = $1.classify.constantize.find(value)
      end
    end
  end

  def comment_redirection
    @commentable || @comment || comments_path
  end

  def set_votable
    @votable = Comment.find(params[:id])
  end
end