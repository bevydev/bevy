class SearchController < ApplicationController
   # before_action :authenticate_user!

  def results
    if user_signed_in?
      @objects = Collection.search params[:search].downcase, index_name: ["items", "collections", "users"]
      @objects = @objects.select {|o| o.is_a?(Collection) ? o.visible_to?(current_user): true}
    else
      @objects = Collection.search params[:search].downcase, index_name: ["items", "collections"]
      @objects = @objects.select {|o| o.is_a?(Collection) ? o.public?: true}
    end

    unless @objects.nil?
      @objects_count = @objects.size()
    else
      @objects_count = 0
    end
  end

  private
    def search_params
      params.require(:search)
    end
end
