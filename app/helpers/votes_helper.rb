module VotesHelper

  def voting_for(votable)
    render partial: "votes/vote",
           locals: {
               votable: votable
           }
  end

  def vote_link(votable, action, *args, &block)
    if block_given?
      name = capture(&block)
      opts = args[0] || {}
    else
      name = args[0]
      opts = args[1] || {}
    end

    url = vote_link_url(votable, action)

    link_opts = {
        remote: true,
        method: :put
    }.merge(opts)

    link_to name, url, link_opts
  end

  def upvote_link(votable, *args, &block)
    action, css_class = :upvote, 'upvote'

    if votable.voted_by?(current_user, true)
      action = :unvote
      css_class << ' ' << 'voted'
    end

    vote_link(votable, action, *args, class: css_class, &block)
  end

  def downvote_link(votable, *args, &block)
    action, css_class = :downvote, 'downvote'

    if votable.voted_by?(current_user, false)
      action = :unvote
      css_class << ' ' << 'voted'
    end

    vote_link(votable, action, *args, class: css_class, &block)
  end

  def vote_link_url(votable, action)
    controller = votable.class.name.pluralize.underscore.to_sym
    url_for(controller: controller, action: action, id: votable.id)
  end

  def upvote_link_url(votable)
    vote_link_url(votable, :upvote)
  end

  def downvote_link_url(votable)
    vote_link_url(votable, :downvote)
  end

  def unvote_link_url(votable)
    vote_link_url(votable, :unvote)
  end

  def load_voting(votable)
    render partial: 'votes/voting', locals: {votable: votable}
  end
end