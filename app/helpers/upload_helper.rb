module UploadHelper
  def image_upload(assoc, field, opts = {})
    o = {
        preview_image: nil
    }.merge!(opts)

    if o[:preview_image].blank?
      o[:preview_image] = nil
    end

    render partial: "upload/upload_options",
           locals: {
               f: assoc,
               field: field,
               preview_image: o[:preview_image],
           }
  end

  def remote_sym_for(field)
    ("remote_" + field.to_s + "_url").parameterize.underscore.to_sym
  end

  def folder_for(field)
    field.to_s.parameterize.underscore.pluralize
  end

  def cache_for(field)
    (field.to_s + "_cache").to_sym
  end

  def remove_sym_for(field)
    ('remove_' + field.to_s).parameterize.underscore.to_sym
  end

  def image_or_placeholder(*args)
    url = args[0]
    url = nil if url.blank?

    options = args[1] || {}

    img_options = {}
    html_options = {}

    options.each do |k, v|
      if [:placeholder].member?(k)
        img_options[k] = v
      else
        html_options[k] = v
      end
    end

    source = url || img_options[:placeholder] || "default_image.png"

    image_tag source, html_options
  end
end