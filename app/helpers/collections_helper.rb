module CollectionsHelper

  def collection_thumb_path(collection, item)
    if item.present?
      new_collection_element_path(collection, item_id: item)
    else
      collection
    end
  end

end
