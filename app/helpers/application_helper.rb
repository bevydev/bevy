module ApplicationHelper
  def user_has_role?(role)
    if Permissions::PermissionManager.has_role? current_user, role
      yield if block_given?
      return true
    end

    false
  end

  def share_button(permissible)
    render partial: 'permissible/share_button', object: permissible
  end
end
