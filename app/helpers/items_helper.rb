module ItemsHelper
  include ElementsHelper

  def element_path(col)
    new_collection_element_path(col, item_id: @item.id)
  end
end
