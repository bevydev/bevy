module UserProfilesHelper
  def resource_name
    :user
  end

  def resource
    @resource = current_user
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def user_avatar(user_profile, version=nil, styles="")
    avatar = user_profile.avatar_url version
    if avatar.nil?
      avatar = "avatar_default.png"
    end
    image_tag avatar, class: styles
  end
end
