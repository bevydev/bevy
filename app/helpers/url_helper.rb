module UrlHelper

  def active_link_to(*args, &block)

    if block_given?
      name = capture(&block)
      options = args[0] || {}
      html_options = args[1] || {}
    else
      name = args[0]
      options = args[1] || {}
      html_options = args[2] || {}
    end

    url = url_for(options)

    active_options = {}
    link_options = {}

    html_options.each do |k, v|
      if [:class_active, :class_inactive].member?(k)
        active_options[k] = v
      else
        link_options[k] = v
      end
    end

    link_css = link_options.delete(:class).to_s + ' '
    link_css << active_link_to_class(url, active_options)
    link_css.strip!

    link_options[:class] = link_css if link_css.present?

    link_to(name, url, link_options)
  end

  def active_link_to_class(url, options = {})
    if is_active_link?(url)
      options[:class_active] || 'selected'
    else
      options[:class_inactive] || ''
    end

  end

  def is_active_link?(url)
    url = url_for(url).sub(/\?.*/, '')
    !request.fullpath.match(/^#{Regexp.escape(url)}\/?(\?.*)?$/).blank?
  end

  def cancel_to(path)
    icon = content_tag(:i, nil, class: "fa fa-reply-all")
    link_to(path, class: "btn btn-default") do
      icon + "Cancel"
    end
  end

  def anchor_for(action, id)
    action.to_s + '_' + id.to_s
  end
end