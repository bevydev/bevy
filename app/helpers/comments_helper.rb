module CommentsHelper

  def comment_content(commentable, comment)
    render partial: "comments/comment",
           locals: {
               commentable: commentable,
               comment: comment
           }
  end

  def comment_replies(commentable, comment)
    render partial: "comments/replies",
           locals: {
               commentable: commentable,
               comment: comment
           }
  end

  def comment_form(commentable, comment)
    render partial: "comments/form",
           locals: {
               commentable: commentable,
               comment: comment
           }
  end

  def comments_list(commentable, comments)
    render partial: "comments/comments",
           locals: {
               commentable: commentable,
               comments: comments
           }
  end

  # TODO Fix comment anchor link

  def comment_anchor_link(comment, html_options = {})
    url = [comment.parent, anchor: anchor_for(:comment, comment.id)]
    html_options[:title] = "Permalink to this comment"
    link_to "#", errors_under_construction_url, class: "ucLink"
  end

end