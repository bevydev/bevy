module ModalHelper
  def my_modal(modal_name, header, &block)
    body = capture(&block)

    html = <<-HTML
    <div class="modal fade" id="#{modal_name}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h1 class="mainpage_header">#{header}</h1>
          </div>
          <div class="modal-body">
            #{body}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    HTML

    html.html_safe
  end
end