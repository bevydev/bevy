module ElementsHelper
  def item_attribute(builder, type)
    case type
      when "date"
        field = "date_field"
      when "decimal"
        field = "number_field"
      when "state"
        field = "check_box"
      else
        field = "text_field"
    end

    content_tag :p do
      safe_join([builder.text_field(:name), builder.send(field, :value), builder.hidden_field(:type)])
    end
  end

  def hide_if(value)
    "display:none" if value
  end

  def generic_attribute(builder)
    render("attribute", f: builder, i: "index_placeholder", attr: (ItemAttribute.new type: "string"))
  end

  def link_to_add_attribute(builder)
    html = generic_attribute(builder)
    label_tag "new_attr", "Add new attribute", class: "btn btn-primary add-attr-btn", onclick: "add_attribute(this, \"#{escape_javascript(html)}\")"
  end
end
