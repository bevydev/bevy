module RelationshipsHelper

  def follow_button(user)
    render partial: 'relationships/follow_button', object: user
  end

  def circle_picker_button(followed_user, following_user)
    render partial: 'circles/circle_picker_button', :locals => {followed_user: followed_user, following_user: following_user }
  end
end
