json.array!(@reports) do |report|
  json.extract! report, :id, :user_id, :issue_type, :title, :content
  json.url report_url(report, format: :json)
end
