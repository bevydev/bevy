class ItemImageUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave

  cloudinary_transformation format: :jpg

  version :standard do
    process resize_to_fit: [800, 800]
  end

  version :thumb, from_version: :standard do
    process resize_to_fill: [300, 300]
  end

  version :medium_thumb, from_version: :thumb do
    process resize_to_fill: [200, 200]
  end

  version :small_thumb, from_version: :thumb do
    process resize_to_fill: [80, 80]
    cloudinary_transformation quality: 80
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

end
