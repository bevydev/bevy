class AvatarUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave

  cloudinary_transformation format: :jpg

  version :standard do
    process resize_to_fit: [800, 800]
  end

  version :thumb, from_version: :standard do
    process resize_to_fill: [300, 300]
  end

  version :small_thumb, from_version: :thumb do
    process resize_to_fill: [34, 34]
    cloudinary_transformation quality: 80
  end

  version :micro_thumb, from_version: :thumb do
    process resize_to_fill: [20, 20]
    cloudinary_transformation quality: 80
  end

  version :comment_thumb, from_version: :thumb do
    process resize_to_fill: [50, 50]
    cloudinary_transformation quality: 80
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

end