// utils

function isNumeric(c)
{
    return !isNaN(c * 1);
}

function isUpperCase(c)
{
    return c == c.toUpperCase();
}

function isLowerCase(c)
{
    return c == c.toLowerCase();
}

function isAllEqual(str)
{
    var equal = true, i = 1;

    while (i < str.length && equal) {
        equal = str[i - 1] == str[i];
        i++;
    }

    return equal;
}

// validation rules implementation

function calcRepetitions(pass)
{
    var occurCounter = 1,
        repetitionCounter = 0;

    for (var i = 1; i < pass.length; i++) {
        if (pass[i] == pass[i - 1]) {
            occurCounter++;
            if (occurCounter > 1) repetitionCounter++;
        } else {
            occurCounter = 1;
        }
    }
    return repetitionCounter;
}

function getPassStrength(pass)
{
    var passRating = {
        "char": 4,
        "upperCase": 2,
        "lowerCase": 2,
        "number": 4,
        "symbol": 6,
        "repetition": 3
    };

    var score = 0, c = '';

    score += pass.length * passRating["char"];

    // additions
    for (var i = 0; i < pass.length; i++) {
        c = pass[i];
        if (isNumeric(c))
            score += passRating["number"];
        else if (isUpperCase(c))
            score += passRating["upperCase"];
        else if (isLowerCase(c))
            score += passRating["lowerCase"];
        else
            score += passRating["symbol"];
    }

    // deductions
    if (isAllEqual(pass)) score = 0;
    score -= calcRepetitions(pass) * passRating["repetition"];

    console.log("Password strength: ", score);

    return score
}

// validation additional rules

function minstrength(value, element, minstrength)
{
    return getPassStrength(value) >= minstrength;
}

function emailCheck(value, element, enabled)
{
    if (enabled) {

    }
}

// validation config

function configValidations()
{
    jQuery.validator.setDefaults({
        debug: false,
        validClass: "valid",
        errorClass: "invalid",
        errorElement: "span",

        errorPlacement: function(error, element) {
            error.insertBefore(element.parents(".form-group").find("label"));
        },
        highlight: function(element, errorClass, validClass) {
            $(element)
                .removeClass(validClass)
                .addClass(errorClass);
            $(element.form)
                .closest("span[for=" + element.id + "]")
                .addClass(errorClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element)
                .removeClass(errorClass)
                .addClass(validClass);
            $(element.form).parent()
                .closest("span[for=" + element.id + "]")
                .removeClass(errorClass);
        },
        submitHandler: function(form) {
            form.submit();
        },
        invalidHandler: function(event, validator) {
            return false;
        }
    });

    jQuery.validator.addMethod("minstrength", minstrength, "Password is too obvious.");
}

$(document).ready(configValidations);
