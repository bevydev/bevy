function switchThumbs(fromThumb, toThumb)
{
    fromThumb.removeClass("voted");
    toThumb.addClass("voted");
}

function resetThumbs(upThumb, downThumb)
{
    upThumb.removeClass("voted");
    downThumb.removeClass("voted");
}