jQuery.fn.extend({
    fadeToggleTo: function(duration, opacity, callback)
    {
        if (this.css("opacity") < 1)
            this.fadeTo(duration, 1, callback);
        else
            this.fadeTo(duration, opacity, callback);
    },

    toggleCheck: function()
    {
        this.prop("checked", !this.prop("checked"));
    }
});