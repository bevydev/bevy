var searchResultsCol = $('#collections-search-results');
$.fn.reverse = [].reverse;


function toggleActiveClass(old,active,activeClass) {
    old.toggleClass(activeClass);
    active.toggleClass(activeClass);
}

function collectionsListResize(){
    var col_num = Math.floor($('.container').width()/220 );
    var size =  Math.floor($('#collections-list .collection-thumb').length / col_num) + 1;
    $('.collections-list').css("height",size*308);
}

function sortAlfaNew(dir,sortableListCol,listSelector,dataSelector){
    sleep(300);
    var listitemsCol = $(listSelector, sortableListCol);
    listitemsCol.sort(function (a, b) {
        return ($(a).data(dataSelector).toUpperCase() > $(b).data(dataSelector).toUpperCase())
    });
    if (dir == -1) listitemsCol.reverse();
    sortableListCol.append(listitemsCol);
}

function sortDateNew(dir,sortableListCol,listSelector,dataSelector){
    sleep(300);
    var listitemsCol = $(listSelector, sortableListCol);
    listitemsCol.sort(function (a, b) {
        return (Date.parse($(a).data(dataSelector)) > Date.parse($(b).data(dataSelector)))
    });
    if (dir == -1) listitemsCol.reverse();
    sortableListCol.append(listitemsCol);
}


function manageAllListActions(){

    var searchResultsCol = $('#collections-search-results');
    var sortableListCol = $('.collections-list');
    var listitemsCol = $('.collection-thumb', sortableListCol);


    $('#sort-reverse-col').click(function(){
        toggleActiveClass($('.active-sort'),$(this),'active-sort');
        searchResultsCol.fadeOut();
        sortAlfaNew(-1,$('.collections-list'),'.collection-thumb','name');
        searchResultsCol.fadeIn();
        return false;
    });

    $('#sort-alfa-col').click(function(){
        toggleActiveClass($('.active-sort'),$(this),'active-sort');
        searchResultsCol.fadeOut();
        sortAlfaNew(1,$('.collections-list'),'.collection-thumb','name');
        searchResultsCol.fadeIn();
        return false;
    });

    $('#sort-date-col').click(function(){
        toggleActiveClass($('.active-sort'),$(this),'active-sort');
        searchResultsCol.fadeOut();
        sortDateNew(-1,$('.collections-list'),'.collection-thumb','created');
        searchResultsCol.fadeIn();
        return false;
    });

    $('#sort-date-rev-col').click(function(){
        toggleActiveClass($('.active-sort'),$(this),'active-sort');
        searchResultsCol.fadeOut();
        sortDateNew(1,$('.collections-list'),'.collection-thumb','created');
        searchResultsCol.fadeIn();
        return false;
    });

    $('#sort-mod-col').click(function(){
        toggleActiveClass($('.active-sort'),$(this),'active-sort');
        searchResultsCol.fadeOut();
        sortDateNew(-1,$('.collections-list'),'.collection-thumb','updated');
        searchResultsCol.fadeIn();
        return false;
    });

    $('#sort-mod-rev-col').click(function(){
        toggleActiveClass($('.active-sort'),$(this),'active-sort');
        searchResultsCol.fadeOut();
        sortDateNew(1,$('.collections-list'),'.collection-thumb','updated');
        searchResultsCol.fadeIn();
        return false;
    });

    // slideshow
    handleCollectionSlideshows();
}

function buildImage(src)
{
    var img = $("<img>", { src: src, "class": "img-responsive" });
    return img;
}

function buildSlideshow(slideshow, urls)
{
    var wrapper = slideshow.parent(".slideshow-wrapper"),
        firstImage = slideshow.find("img").first();

    // prevent slideshow 0 height during initialization
    wrapper.css("width", firstImage.css("width"));
    wrapper.css("height", firstImage.css("height"));

    slideshow.html('');

    $.each(urls, function(index, src) {
        slideshow.append(buildImage(src));
    });

    slideshow.on("cycle-update-view", function() {
        wrapper.css("width", "100%");
        wrapper.css("height", "auto");
    });

    slideshow.cycle({
        fx: "fade",
        timeout: 800,
        speed: 300,
        "auto-height": "calc",
        loader: "wait"
    });
}

function loadSlides(collection, clb)
{
    $.ajax({
        url: collection.data("action"),
        type: "get",
        dataType: "json",
        success: clb
    });
}

function setSlideshow(collection)
{
    var slideshow = collection.find(".slideshow").first(),
        isLoaded = false;

    collection.hover(function() {
        if (isLoaded) {
            slideshow.cycle("resume");
        } else {
            setTimeout(function() {
                if (collection.is(":hover")) {
                    loadSlides(collection, function(slides) {
                        isLoaded = true;
                        if (slides.length > 0)
                            buildSlideshow(slideshow, slides);
                    });
                }
            }, 500);

        }
    }, function() {
        if (isLoaded) {
            slideshow.cycle("pause");
        }
    });
}

function handleCollectionSlideshows()
{
    $(".collections-list").each(function() {
        var collectionsList = $(this);

        collectionsList.find(".collection-thumb").each(function() {
            var collection = $(this);

            setSlideshow(collection);
        });
    });
}

$(document).ready(function(){
    manageAllListActions();
});