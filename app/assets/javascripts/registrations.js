function handleRegistration()
{
    // validation

    var signupEmail = $("#user-email-sign-up"),
        signupUsername = $("#user_username");

    $("#sign-up-form").validate({
        rules: {
            "user[username]": {
                required: true,
                maxlength: 50,
                remote: {
                    url: signupUsername.attr("action"),
                    data: {
                        username: function() {
                            return signupUsername.val();
                        },
                        current_username: function() {
                            return signupUsername.attr("value");
                        }
                    }
                }
            },
            "user[email]": {
                required: true,
                email: true,
                remote: {
                    url: signupEmail.attr("action"),
                    data: {
                        email: function() {
                            return signupEmail.val();
                        },
                        current_email: function() {
                            return signupEmail.attr("value");
                        }
                    }
                }
            },
            "user[password]": {
                required: true,
                minlength: 8,
                minstrength: 50
            },
            "user[password_confirmation]": {
                required: true,
                equalTo: "#user-password-sign-up"
            }
        },
        messages: {
            "user[username]": {
                remote: "Username already in use."
            },
            "user[email]": {
                remote: "Email already in use."
            }
        }
    });

    $("#sign-in-form").validate({
        rules: {
            "user[email]": {
                required: true
            },
            "user[password]": {
                required: true
            }
        }
    });

    $("#user_password").val('');  // TODO Should not be done on client-side

    function isPassPresent() {
        return $("#sign-up-edit-form").find("#user_password").text().length > 0;
    };

    $("#sign-up-edit-form").validate({
        rules: {
            "user[password]": {
                required: false,
                minlength: function() {
                    if (isPassPresent()) return 8;
                    else return false;
                },
                minstrength: function() {
                    if (isPassPresent()) return 50;
                    else return false;
                }
            },
            "user[password_confirmation]": {
                required: isPassPresent(),
                equalTo: "#user_password"
            },
            "user[current_password]": {
                required: true
            }
        }
    });
}

$(document).ready(handleRegistration);

