function perform()
{
    $.event.props.push('dataTransfer');

    // user profile sections toggle

    function reverseCaret(caret) {
        caret.fadeOut("fast", function() {
            $(this).toggleClass("fa-chevron-down");
            $(this).fadeIn("fast");
        })
    }

    function handleSectionToggle(header, content) {
        header.click(function() {
            content.slideToggle("slow", function() {
                reverseCaret(header.find(".chevron"));
            })
        });
    }

    handleSectionToggle($("#up-collections-header"), $("#up-collections"));
    handleSectionToggle($("#up-followers-header"), $("#up-followers"));
    handleSectionToggle($("#up-following-header"), $("#up-following"));

    // collections carousel initialization

    $("#up-collections")
        .find(".item")
        .first()
        .addClass('active');

    $("#up-collections.carousel").carousel({
        interval: false
    });

    // validation

    $("#personal-settings-form").validate({
        rules: {
            "user_profile[name]": {
                maxlength: 50
            },
            "user_profile[description]": {
                maxlength: 1000
            },
            "user_profile[website]": {
                url: true
            }
        }
    });
};

$(document).ready(perform);

