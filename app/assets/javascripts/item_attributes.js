function add_attribute(link, content) {
    var new_id = new Date().getTime();
    var regexp = new RegExp("index_placeholder", "g");
    content = content.replace(regexp, new_id)
    $(link).before(content);
}

function remove_attribute(link) {
    $(link).parents(".item_attribute").remove()
}

function field_type_change(link) {
    var types = ".date_type, .decimal_type, .state_type, .string_type"
    $(link).parents(".item_attribute").find(types).hide()
                                                  .prop("disabled", true)

    var type = "." + $(link).val() + "_type"
    $(link).parents(".item_attribute").find(type).show()
                                                 .prop("disabled", false)
}
function add_attr_template(attrNumber, names, type) {
    for (i = 0; i < attrNumber; i++) {
        $('.add-attr-btn').click();
        attribute = $('.add-attr-btn').prev();
        attribute.addClass("template-attr").find("input[placeholder='Attribute name']").attr("value",names[i]);
        attribute.find("option[value='"+type[i]+"']").attr("selected","selected");
        field_type_change(attribute.find(".type_selector"));
        attribute.find(".cancel-attr").hide();
    }
}

function switch_template(tempName,attrNumber, names, type){
    if (!$("#"+tempName.toLowerCase()+"-category").hasClass("selected"))
    {
        $(".template-attr").remove();
        $("li.selected").toggleClass("selected");
        $($("#"+tempName.toLowerCase()+"-category")).addClass("selected");
        $("#current-template").text(tempName);
        add_attr_template(attrNumber, names, type);
    }
}

$(document).ready(function() {
    $("#film-category").on("click",function(){switch_template("Film",4, ["Genre", "Director", "Release date", "Country"], ["string", "string", "date", "string"])});
    $("#game-category").on("click",function(){switch_template("Game",3, ["Genre", "Producent", "Platform"], ["string", "string", "string"])});
    $("#book-category").on("click",function(){switch_template("Book",3, ["Author", "Pages", "Genre"], ["string", "decimal", "string"])});
    $("#music-category").on("click",function(){switch_template("Music",3, ["Artist", "Release date", "Songs"], ["string", "date", "decimal"])});
    $("#other-category").on("click",function(){switch_template("Other",0, [], [])});
});