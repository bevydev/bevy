function counterOperator() {
    $("[counter]").each(function() {
        var text_area = $(this).wrap('<div class="counter-block"></div>');
        var max = text_area.attr("counter");
        var counter = $("<span></span>", { "class": "char-counter", "unselectable": "on" });
        text_area.after(counter);

        notification_text(text_area, counter, max);
        text_area.on("keydown keyup change", function() {
            notification_text(text_area, counter, max)
        });
    });
}

function notification_text(text_area, element, max) {
    var area = $(text_area);
    var notification = $(element);
    var length = area.val().length;

    if (length > 1.2 * max) {
        area.val(area.val().substring(0, 1.2 * max));
        length = Math.floor(1.2 * max);
    }

    if (length == 0)
        notification.text("");
    else
        notification.text(length.toString() + "/" + max);


    if (length > max) {
        notification.attr("class", "char-counter char-counter-error")
        area.attr("form-valid", false);
    }
    else {
        area.removeAttr("form-valid")
            .removeAttr("title");

        if (length / max > 0.8)
            notification.attr("class", "char-counter char-counter-alert");
        else
            notification.attr("class", "char-counter");
    }
}

$(document).ready(counterOperator);
