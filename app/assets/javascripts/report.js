function handleReport() {

    var selectBox = $("select#report_issue_type");
    var btn = $(".issue-type button .content");
    var dropdown = $(".issue-type .dropdown-menu");

    // initialize dropdown button
    btn.html(dropdown.find("li a[value='" + selectBox.val() + "']").html());

    dropdown.find("li a").click(function() {
        var val = $(this).attr("value");
        var txt = $(this).html();
        selectBox.val(val);
        btn.html(txt);
        return false;
    });

    // validation

    $("#report-form").validate({
        rules: {
            "report[issue_type]": {
                required: true
            },
            "report[title]": {
                required: true,
                maxlength: 50
            },
            "report[content]": {
                required: true,
                maxlength: 500
            }
        }
    });
};

$(document).ready(handleReport);

