function handleBSComponents() {

    // close dropdown after link click

    var dropdowns = $(".dropdown");

    dropdowns.each(function() {
        var drop = $(this);
        var dropToggle = drop.find(".dropdown-toggle");
        var dropMenu = drop.find(".dropdown-menu");

        dropMenu.find("li a").click(function() {
            dropToggle.dropdown("toggle");
        });
    });
}

$(document).ready(handleBSComponents);


