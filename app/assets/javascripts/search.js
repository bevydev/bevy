function searchResize(distance) {
    $('.search-results .media-body').css('width',$('.search-results').width()-distance)
}

$(document).ready(function(){
    $(window).resize(function(){
        searchResize(130);
    });
    searchResize(380);
});
