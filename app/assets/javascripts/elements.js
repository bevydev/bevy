function performElements() {

    $("[data-toggle='tooltip']").tooltip({
        placement: "top"
    });

    // validation

    $("#element-form").validate({
        rules: {
            "element[description]": {
                maxlength: 600
            }
        }
    });
}

$(document).ready(performElements);

