function handleSidebar()
{
    var sidebar = $("#sidebar");
    var pageWrapper = $("#page-wrapper");
    var sidebarBtn = $("#sidebar-btn");
    var sidebarBtnCaret = sidebarBtn.find(".fa");
    var collapseBreakpoint = 800;
    var isCollapsed = false;
    var isToggled = false;

    function initialize() {
        if ($(window).width() < collapseBreakpoint) {
            sidebar.hide();
            switchCaret(sidebarBtnCaret, "right", "fast");
            isCollapsed = true;
        }
        resetPageWrapper();
    }

    function reFade(obj, speed, f) {
        obj.fadeOut(speed, function() {
            f();
            $(this).fadeIn(speed);
        });
    }

    function removeCaret(caret) {
        dirs = ["up", "down", "left", "right"];
        for (var i = 0; i < 4; i++)
            caret.removeClass("fa-chevron-" + dirs[i]);
    }

    function reverseCaret(caret, orientation, speed) {
        reFade(caret, speed, function() {
            if (orientation === "horizontal") {
                caret.toggleClass("fa-chevron-left");
                caret.toggleClass("fa-chevron-right");
            } else if (orientation === "vertical") {
                caret.toggleClass("fa-chevron-up");
                caret.toggleClass("fa-chevron-down");
            }
        });
    }

    function switchCaret(caret, dir, speed) {
        reFade(caret, speed, function() {
            removeCaret(caret);
            caret.addClass("fa-chevron-" + dir);
        });
    }

    function resetPageWrapper() {
        var leftMargin = isCollapsed ? 0 : sidebar.css("width");
        pageWrapper.css("margin-left", leftMargin);
    }

    function toggleSidebar() {
        sidebar.toggleClass("toggled");
        reverseCaret(sidebarBtnCaret, "horizontal", "fast");
        resetPageWrapper();
        isToggled = !isToggled;
    }

    function collapseSidebar() {
        sidebar.toggle();
        reverseCaret(sidebarBtnCaret, "horizontal", "fast");
        searchResize(380);
    }

    sidebarBtn.click(function() {
        if (isCollapsed) {
            collapseSidebar();

        } else {
            if (isToggled)
                searchResize(380);
            else
                searchResize(130);
            toggleSidebar();
        }
    });

    sidebar.find(".sidebar-menu li a").click(function() {
        if (isCollapsed)
            collapseSidebar();
    });

    sidebar.find("#sidebar-search-btn").click(function() {
        toggleSidebar();
        $("#header-search").focus();
    });

    pageWrapper.click(function() {
        if (isCollapsed)
            sidebar.hide();
    });

    $(window).resize(function() {
        var screenWidth = $(window).width();

        if (screenWidth >= collapseBreakpoint) {
            sidebar.show();

            if (isToggled)
                switchCaret(sidebarBtnCaret, "right", "fast");
            else
                switchCaret(sidebarBtnCaret, "left", "fast");

            isCollapsed = false;
        } else if (!isCollapsed) {
            sidebar.hide();
//            if (isToggled) toggleSidebar();
            switchCaret(sidebarBtnCaret, "right", "fast");
            isCollapsed = true;
        }
        resetPageWrapper();
    });

    function handleSectionToggle(header, section) {
        header.click(function() {
            section.slideToggle("fast", function() {
                reverseCaret(header.find(".fa"), "vertical", "fast");
            })
        });
    }

    $("#sidebar-nav").find("h4").each(function() {
        handleSectionToggle($(this), $(this).next(".sidebar-menu"));
    });



    initialize();
}

$(document).ready(handleSidebar);