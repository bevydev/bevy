function scrollBackground(){
    $('.background').each(function(){
        var $bgobj = $(this); // assigning the object

        $(window).scroll(function() {
            var yPos = -(($(window).scrollTop()+$bgobj.data('index')) / $bgobj.data('speed'))

            // Put together our final background position
            var coords = '50% '+ yPos + 'px';

            // Move the background
            $bgobj.css({ backgroundPosition: coords });
        });
    });
}

$(document).ready(scrollBackground);
$(document).on('page:load',scrollBackground);




function scrollToBanner(){
    $("#collect").click(function(){
        var s  = ($(window).height()-$('#bt_0').height())/2;
        $('body').scrollTo('#bj_0',1000,{offset: -s});
        return false;
    });
    $("#share").click(function(){
        var s  = ($(window).height()-$('#bt_1').height())/2;
        $('body').scrollTo('#bj_1',1500,{offset: -s});
        return false;
    });
    $("#discover").click(function(){
        var s  = ($(window).height()-$('#bt_2').height())/2;
        $('body').scrollTo('#bj_2',2000,{offset: -s});
        return false;
    });
    $(".ar_2").click(function(){
        var s  = ($(window).height()-$('#bt_2').height())/2;
        $('body').scrollTo('#bj_2',1000,{offset: -s});
        return false;
    });
    $(".ar_1").click(function(){
        var s  = ($(window).height()-$('#bt_1').height())/2;
        $('body').scrollTo('#bj_1',1000,{offset: -s});
        return false;
    });
    $(".ar_0").click(function(){
        $('body').scrollTo('.main_photo',1200);
        return false;
    });
}

$(document).ready(scrollToBanner);

