function userOperate() {

    //TODO consider hiding/showig this after adding endlessscroll and pagintation
    $('#users-search-results-count').hide();

    $('#users-search input').bind("keyup click", function () {
        var text = $('#users-search').serialize();
        $.get($('#users-search').attr('action'),
            text, null, 'script');
        return false;
    });

    $('#users-search input').click();

    $('#user_search').autocomplete({
        source: '/user_profiles/autocomplete.json'
    });
}


$(document).ready(userOperate);


