var searchResultsItems = $('.collection-view .active .main-list');

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}

function listResize() {
    $('#sortable .media-body').css('width',$('#sortable').width()-130)
}

function shelfResize(){
    var col_num = Math.floor($('#page-wrapper').width()/90 );
    var size =  Math.floor($('.items-shelf li').length / col_num) + 1;
    $('.items-shelf').css("height",size*108);
}

function switchCollectionView(button,selectedView,oldViews) {
    toggleActiveClass($('.active-view'),button,'active-view');
    $('.active-sort').removeClass('active-sort');
    oldViews[0].fadeOut(400).removeClass('active');
    oldViews[1].fadeOut(400).removeClass('active');
    selectedView.delay(400).fadeIn(400).addClass('active');
    searchResultsItems = $('.collection-view .active .main-list');
}

function manageCollectionButtons(){
    $('#collection-shelf').on("click", function() {
        switchCollectionView($(this),$('.collection-grid'),[ $('.collection_listed'),$('.collection_carousel')]);
        return false;
    });

    $('#collection-carousel').on("click",function(){
        switchCollectionView($(this),$('.collection_carousel'),[ $('.collection_listed'),$('.collection-grid')]);
        return false;
    });

    $('#collection-list').on("click",function(){
        switchCollectionView($(this),$('.collection_listed'),[ $('.collection-grid'),$('.collection_carousel')]);
        return false;
    });
}

function manageAllActions(){
    shelfResize();
    $(window).resize(function(){
        shelfResize();
        listResize();
    });



    $('.items-shelf').sortable({revert: true, containment: '.items-shelf'}).disableSelection();
    $('#sortable').sortable({revert: true, containment: '#sortable'}).disableSelection();
    $('.items_carousel').sortable({revert: true, containment: '.items_carousel'}).disableSelection();

    searchResultsItems = $('.collection-view .active .main-list');

    $('#sort-reverse').click(function(){
        toggleActiveClass($('.active-sort'),$(this),'active-sort');
        searchResultsItems.fadeOut();
        sortAlfaNew(-1,$('.collection-view .active .main-list'),'li.item','name');
        searchResultsItems.fadeIn();
        return false;
    });

    $('#sort-alfa').click(function(){
        toggleActiveClass($('.active-sort'),$(this),'active-sort');
        searchResultsItems.fadeOut();
        sortAlfaNew(1,$('.collection-view .active .main-list'),'li.item','name');
        searchResultsItems.fadeIn();
        return false;
    });

    $('#sort-date').click(function(){
        toggleActiveClass($('.active-sort'),$(this),'active-sort');
        searchResultsItems.fadeOut();
        sortDateNew(-1,$('.collection-view .active .main-list'),'li.item','created');
        searchResultsItems.fadeIn();
        return false;
    });

    $('#sort-date-rev').click(function(){
        toggleActiveClass($('.active-sort'),$(this),'active-sort');
        searchResultsItems.fadeOut();
        sortDateNew(1,$('.collection-view .active .main-list'),'li.item','created');
        searchResultsItems.fadeIn();
        return false;
    });

    $('#sort-mod').click(function(){
        toggleActiveClass($('.active-sort'),$(this),'active-sort');
        searchResultsItems.fadeOut();
        sortDateNew(-1,$('.collection-view .active .main-list'),'li.item','updated');
        searchResultsItems.fadeIn();
        return false;
    });

    $('#sort-mod-rev').click(function(){
        toggleActiveClass($('.active-sort'),$(this),'active-sort');
        searchResultsItems.fadeOut();
        sortDateNew(1,$('.collection-view .active .main-list'),'li.item','updated');
        searchResultsItems.fadeIn();
        return false;
    });

    manageCollectionButtons();

    $('.item-details').click(function(){
        $(this).toggleClass("fa-caret-down").toggleClass("fa-caret-up");
        $(this).next().slideToggle();
        return false;
    })
}

$(document).ready(function(){
    manageAllActions();
});

