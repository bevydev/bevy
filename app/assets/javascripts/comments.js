function setCommentForm(form)
{
    var textarea = form.find("textarea"),
        submitBtn = form.find("input[type=submit]"),
        cancelBtn = form.find(".cancel"),
        replyBtn = form.parents(".comment").find("a.reply");

    setSubmitBtn(submitBtn, textarea);

    textarea.keyup(function() {
        setSubmitBtn(submitBtn, textarea);
    });

    cancelBtn.click(function() {
        textarea.val('');
        form.hide();
        replyBtn.show();
    });

    form.submit(function() {
        setSubmitBtn(submitBtn, textarea);
    });
}

function setReplyBtn(btn)
{
    var form = btn.parents('.comment').find(".form");

    btn.off("click").click(function() {
        btn.hide();
        setCommentForm(form);
        form.toggle();
        return false;
    });
}

function setSubmitBtn(btn, textarea)
{
    if (isEmpty(textarea))
        btn.attr("disabled", "disabled");
    else
        btn.removeAttr("disabled");
}

function isEmpty(textarea) { return !textarea.val().length > 0 }

function handleComments()
{
    $(".comment.form").each(function() {
        setCommentForm($(this));
    });
}

$(document).ready(handleComments);