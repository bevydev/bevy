function handleUpload()
{
    var uploadOpts = $(".upload-opts");

    uploadOpts.each(function () {
        var opts = $(this),
            uploadInput = opts.find(".cloudinary-fileupload"),
            preview = opts.find(".u-preview"),
            progress = opts.find(".u-progress"),
            progressBar = progress.find(".u-progress-bar"),
            status = opts.find(".u-status"),
            form = opts.parents("form"),
            submitBtn = form.find("input[type=submit]"),
            deleteToken = null;

        var remoteBtn = opts.find(".u-remote"),
            remoteInput = opts.find(".u-remote-input");

        // direct upload

        uploadInput
            .cloudinary_fileupload({
                dropZone: uploadInput,
                autoUpload: true,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 5000000,                           // 5 MB
                maxNumberOfFiles: 1,
            })
            .on("fileuploadadd", function(e, data) {
                remoteInput.val('');
            })
            .on("fileuploadstart", function(e, data) {
                progress.fadeIn("fast");
                submitBtn.attr("disabled", "disabled");
            })
            .on("fileuploadprogress", function(e, data) {
                var proc = Math.round(data.loaded * 100.0 / data.total);
                progressBar.width(proc + '%');
            })
            .on("fileuploadfail", function(e, data) {
                submitBtn.removeAttr("disabled");
                printStatus(status, "Upload failed");
            })
            .off("cloudinarydone")
            .on("cloudinarydone", function(e, data) {
                if (deleteToken != null)
                    $.cloudinary.delete_by_token(deleteToken);
                deleteToken = data.result.delete_token;

                submitBtn.removeAttr("disabled");

                var uploadedImage = $.cloudinary.image(data.result.public_id);
                uploadedImage.hide();

                preview.find("img").fadeOut("slow", function () {
                    $(this).remove()
                    preview.append(uploadedImage);
                    uploadedImage.fadeIn("slow");
                });

                progress.fadeOut("fast");
                progressBar.width(0 + '%');
            });

        // remote upload
        var remoteGroup = remoteInput.parent(".form-group");
        remoteGroup.hide();

        opts.find("li").not(remoteBtn).click(function() {
            remoteGroup.fadeOut("slow");
        })

        remoteBtn.click(function() {
            remoteGroup.fadeToggle("slow", function() {
                $(this).focus();
            });
        });

        // handle remove undertakers

        var removeBtn = preview.find(".undertaker.remove"),
            removeCheck = preview.find("input[type=checkbox].remove");

        removeBtn.click(function() {
            preview.find("img").fadeToggleTo("slow", 0.5, function() {
                removeCheck.toggleCheck();
            });

            return false;
        });

        // validation

        remoteInput.rules("add", {
            url: true
        })
    });

    // disable the default browser action for file drops on the document

    $(document).on("drop dragover", function () {
        return false;
    });

    function printStatus(status, msg)
    {
        status.hide();
        status.html(msg);

        status.fadeIn("fast", function() {
            setInterval(function() {
                status.fadeOut("fast");
            }, 5000);
        });
    }
}

$(document).ready(handleUpload);
