function handleCircles() {
    $(".circle-picker").on('click', ".checkable", function () {
        $(this).parents('form:first').submit();
    });

    $(".circle").click(function () {
        $('.circle').removeClass("highlighted");
        $(this).addClass("highlighted");
    });

    // validation

    $("#circle-form").validate({
        rules: {
            "circle[name]": {
                required: true,
                maxlength: 64
            }
        }
    });
}

$(document).ready(handleCircles);
