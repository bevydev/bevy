function collectionOperate() {
    $('#collections-search-results-count').hide();

    function makeChanges() {
        var text = $('#collections-search').serialize() + '&' + $('#sort-selector').serialize() + '&' + $('#direction-selector').serialize();
        if($('#item-id').val()) {
            text = text + '&' + $('#item-id').serialize();
        }

        $.get($('#collections-search').attr('action'),
            text,function(){
                collectionsListResize();
                $(window).resize(function(){
                    collectionsListResize();
                });
                $('.collections-list').sortable({revert: true, containment: '.collections-list'});
            }, 'script');
    }

    $('#collections-search input').bind("keyup click", function () {
        makeChanges();
        return false;
    });

    $('#collections-search input').click();

    $('#collection_search').autocomplete({
        source: '/collections/autocomplete.json'
    });

    $('#sort-group select').change(function () {
        makeChanges();
    });

    // validation

    $("#collection-form").validate({
        rules: {
            "collection[name]": {
                required: true,
                remote: {
                    url: $("#collection_name").attr("action"),
                    data: {
                        name: function() {
                            return $("#collection_name").val();
                        },
                        user_id:  function() {
                            return $("#user_id").val();
                        },
                        current_name: function() {
                            return $("#collection_name").attr("value");
                        }
                    }
                },
                maxlength: 50
            },
            "collection[description]": {
                maxlength: 600
            }
        },
        messages: {
            "collection[name]": {
                remote: "Name already in use."
            }
        }
    });
}

$(document).ready(collectionOperate);



