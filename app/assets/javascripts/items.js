function itemOperate() {

    //TODO consider hiding/showig this after adding endlessscroll and pagintation
    //$('#items-search-results').hide();
    $('#items-search-results-count').hide();

    $('#items-search input').bind("keyup click", function () {
        var text = $('#items-search').serialize();
        if($('#collection-id').val()) {
            text = text + '&' + $('#collection-id').serialize();
        }
        $.get($('#items-search').attr('action'),
            text, null, 'script');
       return false;
    });

    $('#items-search input').click();

    $('#item_search').autocomplete({
       source: '/items/autocomplete.json'
    });


    // validation

    $("#item-form").validate({
        rules: {
            "item[name]": {
                required: true,
                maxlength: 50
            },
            "item[description]": {
                maxlength: 600
            }
        }
    });
}


$(document).ready(itemOperate);

$(document).ready(function(){
    $("a.fancybox").fancybox({
        'speedIn'		:	500,
        'speedOut'		:	200,
        'padding'       :   0,
        'centerOnScroll' :  true
    });
});



