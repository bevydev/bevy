function handleHeaderSearch() {

    var searchForm = $("#search-form");
    var searchInput = $("#header-search");
    var searchResults = $("#layout-search-results");
    var action = searchInput.attr("action");

    searchInput.on("keyup click", function() {
        searchResults.show();
        var query = searchInput.serialize();
        query = query.slice(7, query.length);

        searchForm.submit();

        if (query != "") {
            searchResults.show();
        }
        else {
            searchResults.hide();
        }

        return false;
    });

    searchInput.on("keyup keydown keypress", function(e) {
        if (e.keyCode == 13) {
            var query = searchInput.serialize();
            query = query.slice(7, query.length);
            if (query != "") {
                window.location = "/search/results?search=" + query;
            }
        }
    });

    $(document).click(function(e) {
        if(!searchResults.find(e.target).length) {
            searchResults.fadeOut("fast");
        }
    });
}

$(document).ready(handleHeaderSearch);
