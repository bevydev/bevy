module AdmittableGroup
  extend ActiveSupport::Concern
  include Admittable

  @@classes = []

  def self.included(base)
    base.has_many :social_permits, as: :admittable
    @@classes << base
  end

  def self.groups_for_single_admittable(arguments = {})
    raise NotImplementedError
  end

  def self.all_groups_for(arguments = {})
    assert_correct_arguments(arguments)
    groups = Set.new
    @@classes.each do |c|
      c.groups_for_single_admittable(what: arguments[:what], to: arguments[:to]).each { |group| groups.add(group) }
    end
    groups
  end

  def can_see?(permissible)
    permissible.visible_to?(self)
  end

  def permit(permissible)
    SocialPermit.permit(what: permissible, to: self) unless self == permissible.owner
  end

  def prohibit(permissible)
    SocialPermit.prohibit(what: permissible, to: self) unless self == permissible.owner
  end



  private

  def self.all_admittable_groups
    @@classes
  end

  def self.assert_correct_arguments(arguments = {})
    arguments.assert_valid_keys(:what, :to)
    raise ArgumentError.new unless arguments[:what].is_a?(Permissible) && arguments[:to].is_a?(Admittable)
  end
end