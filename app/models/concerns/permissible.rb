module Permissible
  extend ActiveSupport::Concern

  included do
    has_many :social_permits, as: :permissible
  end

  def visible_to?(admittable)
    return true if self.owner == admittable
    case SocialPermit.permit_type_for(what: self, to: admittable)
      when :prohibited
        return false
      when :public
        return true
      when :permitted
        return true
      else
        return false
    end
  end

  def public?
    SocialPermit.publicly_permitted?(self)
  end

  def owner
    self.user
  end

  #checks explicit permission/prohibition, ignores public permission
  def prohibited_to?(admittable)
    return true if SocialPermit.permit_type_for(what: self, to: admittable) == :prohibited
    false
  end

  def permitted_to?(admittable)
    return true if SocialPermit.permit_type_for(what: self, to: admittable) == :permitted
    false
  end

  def permit_to(admittable)
    SocialPermit.permit(what: self, to: admittable) unless self.owner == admittable
  end

  def prohibit_to(admittable)
    SocialPermit.prohibit(what: self, to: admittable) unless self.owner == admittable
  end

  def permit_publicly
    SocialPermit.permit_publicly(self)
  end

  def prohibit_publicly
    SocialPermit.prohibit_publicly(self)
  end

end