module Admittable
  extend ActiveSupport::Concern

  included do
    has_many :social_permits, as: :admittable
  end

  def can_see?(permissible)
    permissible.visible_to?(self)
  end

  def permit(permissible)
    SocialPermit.permit(what: permissible, to: self) unless self == permissible.owner
  end

  def prohibit(permissible)
    SocialPermit.prohibit(what: permissible, to: self) unless self == permissible.owner
  end


end