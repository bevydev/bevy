module Votable
  extend ActiveSupport::Concern

  included do
    has_many :votes, :as => :votable, :dependent => :destroy
  end

  def vote_by(voter, value = true)
    votes = Vote.where(user: voter, votable: self)

    if votes.count == 0
      vote = Vote.new(user: voter, votable: self)
    else
      vote = votes.last
    end

    vote.value = value
    vote.save
  end

  def upvote_by(voter)
    vote_by(voter, true)
  end

  def downvote_by(voter)
    vote_by(voter, false)
  end

  def unvote_by(voter)
    votes = Vote.where(user: voter, votable: self)
    votes.each do |v| v.destroy end
  end

  def votes_for
    self.votes.where(value: true).count
  end

  def votes_against
    self.votes.where(value: false).count
  end

  def voting_score
    votes_for - votes_against
  end

  def voted_by?(voter, value = nil)
    if value.nil?
      Vote.exists?(user: voter, votable: self)
    else
      Vote.exists?(user: voter, votable: self, value: value)
    end

  end
end