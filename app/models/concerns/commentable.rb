module Commentable
  extend ActiveSupport::Concern

  included do
    has_many :comments, :as => :commentable, :dependent => :destroy
  end

  def comments_ordered
    self.comments.all.order(created_at: :desc)
  end
end