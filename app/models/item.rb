class Item < ActiveRecord::Base
  include Commentable

  has_many :collections, through: :elements
  has_many :elements
  validates :description, length: { maximum: 600 }
  validates :name, presence: true, length: { maximum: 50 }
  belongs_to :user

  mount_uploader :image, ItemImageUploader

  searchkick  autocompletable: [:name], index_name: "items", merge_mappings: true, mappings: {
      item: {
          properties: {
              name: {type: "string"}
          }
      }
  }

  validates :name, :presence => true

  def self.search_item_auto(term)
    first = Item.search(query: {prefix: {name: term}}, limit: 4).map(&:name)
    second = Item.search(term, autocompleteable: true, limit: 2, misspellings: {edit_distance: 5}).map(&:name)
    second.each { |item| first << item unless first.include?(item) }
    first
  end

  def search_data
    {
        name: name
    }
  end

  def all_attributes
    ItemAttribute.where item: self, user_id: SPECIAL_USER_ID
  end

  def comment_visible_to?(user)
    true
  end
end
