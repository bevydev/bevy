class User < ActiveRecord::Base
  include Admittable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :collections, dependent: :destroy
  has_many :user_roles
  has_many :roles, through: :user_roles
  has_one :user_profile
  has_many :comments
  has_many :votes

  before_destroy :remove_attributes

  has_many :reports
  has_many :relationships, foreign_key: "following_user_id", dependent: :destroy
  has_many :followed_users, through: :relationships, source: :followed_user
  has_many :reverse_relationships, foreign_key: "followed_user_id", class_name: "Relationship", dependent: :destroy
  has_many :followers, through: :reverse_relationships, source: :following_user
  has_many :circles

  validates :username, presence: true, length: { maximum: 50 }
  validates :username, uniqueness: true#, if: -> { self.username.present? }

  # each new user should have public circle, which can't be modified/deleted
  after_create :make_public_circle!
  before_destroy :remove_attributes

  def remove_attributes
    (ItemAttribute.where user: self).each { |a| a.record.destroy }
  end

  def role_names
    @role_names ||= self.roles.map(&:name)
  end

  def add_role(role)
    self.roles << Role.find_or_create_by_name(role)
  end

  def self.username_available?(name)
    !User.exists?(username: name)
  end

  def self.email_available?(email)
    !User.exists?(email: email)
  end

  def make_public_circle!
    Circle.create!(user: self, level: 0, name: "Public") unless self.public_circle
  end

  def public_circle
    self.circles.find_by_level(0)
  end

  def is_following?(other_user)
    return true if relationships.find_by(followed_user_id: other_user.id)
    false
  end

  def follow!(other_user)
    if other_user && other_user != self && !self.is_following?(other_user)
      Relationship.create!(following_user: self, followed_user: other_user, circle: other_user.public_circle)
    end
  end

  def unfollow!(other_user)
    # rel = Relationship.where(followed_user_id: other_user.id, following_user_id: self.id)
    # rel.each do |r|
    #   r.destroy
    # end
    relationships.where(followed_user_id: other_user.id, following_user_id: self.id).destroy_all
  end

  def add_to_circle!(other_user, circle)
    Relationship.create!(following_user: other_user, followed_user: self, circle: circle)
  end

  searchkick  autocompletable: [:username], index_name: "users", merge_mappings: true, mappings: {
      user: {
          properties: {
              username: {type: "string"}
          }
      }
  }

  def self.search_user_auto(term)
    first = User.search(query: {prefix: {username: term}}, limit: 4).map(&:username)
    second = User.search(term, autocompleteable: true, limit: 2, misspellings: {edit_distance: 5}).map(&:username)
    second.each { |user| first << user unless first.include?(user) }
    first
  end

  def search_data
    {
        username: username
    }
  end

  def profile
    UserProfile.find_by(user_id: self.id)
  end

  def name_or_username
    if self.profile.name.present?
      self.profile.name
    else
      self.username
    end
  end
end
