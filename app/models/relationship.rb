class Relationship < ActiveRecord::Base
  include PublicActivity::Model
  tracked except: [:destroy,:update],owner: ->(controller, model) { controller && controller.current_user }
  belongs_to :following_user, class_name: "User"
  belongs_to :followed_user, class_name: "User"
  belongs_to :circle

  #validates :following_user_id, presence: true, uniqueness: { scope: :followed_user_id}
  validates :followed_user_id, presence: true
  validates :circle, presence: true

  def activity_visible_to?(user)
    self.followed_user == user
  end
end
