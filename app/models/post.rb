class Post < ActiveRecord::Base
  belongs_to :user
  validates :title,
            :presence => true,
            :length => { maximum: 100 }
  validates :publication, :presence => true
  validates :content,
            :presence => true,
            :length => { maximum: 10000 }
end
