class Collection < ActiveRecord::Base
  include Permissible
  include PublicActivity::Model
  
  tracked except: :destroy,owner: ->(controller, model) { controller && controller.current_user }

  belongs_to :user
  has_many :items, through: :elements
  has_many :elements, dependent: :destroy
  
  validates :user_id, presence: true
  validates :name, presence: true, :uniqueness => { scope: :user_id }, length: { maximum: 50 }
  validates :user, presence: true
  validates :description, length: { maximum: 600 }


  searchkick  autocompletable: [:name], index_name: "collections", merge_mappings: true, mappings: {
      collection: {
          properties: {
              name: {type: "string"}
          }
      }
  }

  def search_data
    {
        name: name,
        description: description,
        created: created_at,
        updated: updated_at,
        user: user_id
    }
  end

  def find_element_item_pairs
    self.elements.includes :item
  end

  def self.search_collection_auto(term, user)
    first = Collection.search(query: {prefix: {name: term}}, where: {user: user}, limit: 3).map(&:name)
    second = Collection.search(term, field: ["name"], where: {user: user}, autocompleteable: true, limit: 2, misspellings: {edit_distance: 5}).map(&:name)
    second.each { |collection| first << collection unless first.include?(collection) }
    first
  end

  def self.find_available_collections(user_id, item)
    all = Collection.where(user_id: user_id).includes(:elements).order(updated_at: :desc)
    result = []

    all.each do |collection|
      result << collection unless collection.elements.any? { |e| e.item == item }
    end
    result
  end

  def self.name_available?(name, user_id)
    !Collection.exists?(name: name, user_id: user_id)
  end

  def activity_visible_to?(user)
    self.visible_to?(user)
  end

  def cover_image
    element = self.elements.find { |e| e.get_image.present? }
    element.get_image.thumb.url unless element.nil?
  end

  def slideshow
    elements = self.elements.select { |e| e.get_image.present? }
    slideshow = elements.map { |e| e.get_image.thumb.url }
  end
end