class UserRole < ActiveRecord::Base
  # integer: user_id, integer: role_id
  belongs_to :user
  belongs_to :role

  validates :user_id, :presence => true
  validates :role_id, :presence => true

  validates :user, :presence => true
  validates :role, :presence => true

  validates :role_id, :uniqueness => { :scope => :user_id}
end
