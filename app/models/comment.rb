class Comment < ActiveRecord::Base
  include Votable
  include PublicActivity::Model
  
  tracked except: [:destroy,:update],owner: ->(controller, model) { controller && controller.current_user }
  
  belongs_to :commentable, :polymorphic => true
  belongs_to :user
  has_many :comments, :as => :commentable

  validates :message,
            :presence => true,
            :length => {maximum: 1024}
  validates :user,
            :presence => true
  validates :commentable,
            :presence => true

  def author
    User.find(self.user_id).username
  end

  def avatar
    UserProfile.find_by(user_id: self.user_id).avatar
  end

  def reply?
    self.commentable.is_a?(Comment)
  end

  def parent
    if self.commentable.is_a?(Comment)
      self.commentable.parent
    else
      self.commentable
    end
  end

  def activity_visible_to?(user)
    self.commentable ? self.commentable.comment_visible_to?(user) : false
  end

  def comment_visible_to?(user)
    self.commentable && self.commentable.is_a?(Item) ? true : self.commentable.collection.visible_to?(user)
  end
end