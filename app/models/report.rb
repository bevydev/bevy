class Report < ActiveRecord::Base
  belongs_to :user

  validates :user_id, :issue_type, :title, :content, presence: true
  validates :title, length: {maximum: 50}
  validates :content, length: {maximum: 500}
end
