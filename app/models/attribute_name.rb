class AttributeName < ActiveRecord::Base
  validates :name, :presence => true, :uniqueness => true
end

