class UserProfile < ActiveRecord::Base
  include PublicActivity::Model
  tracked except: [:destroy,:create],owner: ->(controller, model) { controller && controller.current_user }
  belongs_to :user
  validates :name, length: { maximum: 50 }
  validates :description, length: { maximum: 1000 }

  mount_uploader :avatar, AvatarUploader

  def activity_visible_to?(user)
    true
  end
end
