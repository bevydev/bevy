class StringAttribute < ActiveRecord::Base
  belongs_to :user
  belongs_to :item
  belongs_to :attribute_name

  validates :attribute_name, presence: true

  def name
    self.attribute_name.name
  end

  def name=(value)
    self.attribute_name = AttributeName.find_or_create_by name: value
    self.save
  end

  def self.parse_value(val)
    val
  end
end
