class ItemAttribute
  include ActiveModel::Model

  validates :item, presence: true
  validates :user, presence: true
  validates :name, presence: true
  validate :name_is_unique_for_user?
  validates :type, inclusion: { in: ATTRIBUTE_TYPES }

  attr_accessor :item, :item_id, :user, :user_id, :name, :attribute_name, :value, :type, :overridden, :record, :destroyed

  def self.convert_record(record)
    ItemAttribute.new type: get_attribute_type(record),
      item_id: record.item_id,
      user_id: record.user_id,
      name: record.attribute_name,
      value: record.value,
      record: record
  end

  def self.where(options = {})
    result = []

    tables do |table|
      records = table.where(options).includes(:attribute_name).limit(32)
      result += records.map { |r| ItemAttribute.convert_record r }
    end

    result
  end

  def self.get_all(options = {})
    if options[:user_id]
      uid = options[:user_id]
    else
      uid = options[:user].id
    end

    if options[:item_id]
      iid = options[:item_id]
    else
      iid = options[:item].id
    end

    defaults = ItemAttribute.where user_id: SPECIAL_USER_ID, item_id: iid
    user_attr = ItemAttribute.where user_id: uid, item_id: iid
    not_overridden = []

    defaults.each do |default|
      overridden_by = user_attr.select { |a| a.name == default.name }
      raise "Database error: more than one default attribute with name #{default.name}" if overridden_by.size > 1

      if overridden_by.empty?
        not_overridden << default
      else
        overridden_by.first.overridden = default
      end
    end

    not_overridden.each do |attr|
      user_attr << (ItemAttribute.new overridden: attr)
    end

    user_attr
  end

  def save
    return false unless @record.nil?
    n = @attribute_name.nil? ? @overridden.attribute_name : @attribute_name

    if valid?
      @record = table_name.create attribute_name: n, value: @value, user_id: @user_id, item_id: @item_id
      true
    else
      self.item.errors[:base] << self.errors.to_s
      false
    end
  end

  def self.update(user_id, item_id, new_attrs)
    return ItemAttribute.redo_attr(item_id, new_attrs) if user_id == SPECIAL_USER_ID

    current_records = ItemAttribute.get_all(user_id: user_id, item_id: item_id)
    current_records.each do |attr|
      new_value = new_attrs.select { |a| a.name == attr.name }
      new_value = new_value.empty? ? nil : new_value.first

      # this attribute will be handled here
      new_attrs.delete(new_value) if new_value

      # ignore if no change requested
      next if (new_value && (new_value.value == attr.value))

      if new_value # user requested some change on already existing attribute
        # if current attribute value is of the default attribute, new personal attribute needs to be created
        attr.override!(user_id, new_value.value, new_value.type) and next if attr.default_attribute?
        attr.record.update(value: new_value.value) and next if attr.overridden? || attr.new_attribute?
      else                                                                # there is attribute in the database that needs to be removed
        attr.record.destroy and next if attr.new_attribute?               # if there is no default attribute, just delete personal attribute
        attr.record.update(value: nil) and next if attr.overridden?       # if the default attribute is overridden, change personal attribute's value to NULL
        attr.override!(user_id, nil) and next if attr.default_attribute?           # if the default attribute needs to be removed, create new personal attribute
      end
    end

    new_attrs.each do |attr|
      attr.save
    end
  end

  # special attribute methods
  def item
    @item = Item.find(@item_id) unless @item
    @item
  end

  def item=(arg)
    set_item item: arg
  end

  def item_id=(arg)
    set_item item_id: arg
  end

  def user
    @user = User.find(@user_id) unless @user
    @user
  end

  def user=(arg)
    set_user user: arg
  end

  def user_id=(arg)
    set_user user_id: arg
  end

  def name
    return @attribute_name.name unless @attribute_name.nil?
    @overridden.nil? ? nil : @overridden.name
  end

  def name=(arg)
    if arg.class == String
      @attribute_name = AttributeName.find_or_create_by(name: arg)
    elsif arg.class == AttributeName
      @attribute_name = arg
    else
      raise "Argument Exception in #{self.class}#name="
    end
  end

  def value
    return @value unless default_attribute?
    @overridden.nil? ? nil : @overridden.value
  end

  def value=(arg)
    @value = arg
    cast_value
  end

  def type
    @type.nil? ? @overridden.type : @type
  end

  def type=(arg)
    raise "Unknown type #{arg}" unless ATTRIBUTE_TYPES.include? arg
    @type = arg
    cast_value
  end

  def destroy
    self.record.destroy if self.record
    @destroyed = true
  end

  def destroyed=
    self.destroy
  end

  # copies data from default attribute, preparing attribute for saving with overridden value optionally passed as param
  def override!(user_id = self.user_id, value = self.value, type = self.type)
    return false if @overridden.nil?

    self.name = @overridden.attribute_name
    self.value = value
    self.user_id = user_id
    self.item_id = @overridden.item_id
    self.type = type

    self.save
  end

  def overridden?
    !(@overridden.nil? || @attribute_name.nil?)
  end

  def default_attribute?
    !@overridden.nil? && @attribute_name.nil?
  end

  def new_attribute? # no default
    @overridden.nil? && !@attribute_name.nil?
  end

  def self.parse(user_id, item_id, params)
    return [] if params.nil? || params[:attributes].nil?
    result = []

    params[:attributes].each_value do |value|
      next if value[:name].blank?
      next if !value[:value].nil? && value[:value].blank?

      value[:value] = false if value[:type] == "state" && value[:value].nil?
      result << (ItemAttribute.new value.merge({ item_id: item_id, user_id: user_id }))
    end

    result
  end

  private
  def cast_value
    return nil if (@value.nil? || @type.nil? || !@value.is_a?(String))

    @value = table_name.parse_value(@value)
  end

  def set_item(options = {})
    @item = options[:item]

    if @item
      @item_id = @item.id
    else
      @item_id = options[:item_id]
    end
  end

  def set_user(options = {})
    @user = options[:user]

    if @user
      @user_id = @user.id
    else
      @user_id = options[:user_id]
    end
  end

  def self.get_attribute_type(record)
    record.class.to_s[0..-10].downcase
  end

  def self.tables
    tables = ATTRIBUTE_TYPES.map { |a| table_name(a) }
    tables.each { |t| yield(t) } if block_given?
    tables
  end

  def self.table_name(type)
    (type + "Attribute").camelize.constantize
  end

  def table_name
    ItemAttribute.table_name(@type)
  end

  def name_is_unique_for_user?
    attr = ItemAttribute.where(item_id: self.item_id, user_id: @user_id, attribute_name: @attribute_name)
    if attr.size != 0
      errors[:base] << "ItemAttribute with name #{name} already exists in database and has value #{attr.first.value}"
    end
  end

  def ItemAttribute.redo_attr(item_id, attributes)
    ItemAttribute.where(user_id: SPECIAL_USER_ID, item_id: item_id).each { |r| r.destroy }
    result = true
    attributes.each { |a| result &&= a.save }

    result
  end
end