class Circle < ActiveRecord::Base
  include AdmittableGroup

  belongs_to :user
  has_many :relationships
  has_many :followers, through: :relationships, source: :following_user

  before_destroy :remove_relationships
  before_destroy :check_public
  before_update :check_public

  validates :name, uniqueness: { scope: :user_id }, presence: true, length: { maximum: 64 }
  # validates :level, uniqueness: { scope: :user_id }, presence: true, numericality: { greater_than_or_equal_to: 0 }

  def remove_relationships
    (Relationship.where circle: self).each { |r| r.destroy }
  end

  # public circle can't be modified/deleted
  def check_public
    false if self.level == 0
  end

  def self.groups_for_single_admittable(arguments = {})
    AdmittableGroup.assert_correct_arguments(arguments)
    Circle.occupied_circles(arguments[:what].owner, arguments[:to])
  end

  def self.occupied_circles(owner, follower)
    owner.circles.select { |circle| circle.followers.include?(follower)}
  end
end
