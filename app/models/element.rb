class Element < ActiveRecord::Base
  include Commentable
  include PublicActivity::Model
  tracked except: :destroy,owner: ->(controller, model) { controller && controller.current_user }

  belongs_to :collection
  belongs_to :item

  validates :item_id, :presence => true, uniqueness: { scope: :collection }
  validates :collection_id, :presence => true

  validates :item, :presence => true
  validates :collection, :presence => true

  validates :item_id, :uniqueness => { :scope => :collection_id}

  validates :description, length: { maximum: 600 }

  mount_uploader :image, ItemImageUploader

  def link(collection, item)
    self.collection = collection
    self.item = item
  end

  def name
    self.item.name
  end

  def get_image
    if self.image.blank?
      self.item.image
    else
      self.image
    end
  end

  def item_attributes
    ItemAttribute.get_all user_id: owner_id, item_id: item_id
  end

  # ovverride description getter with special logic
  def description
    attributes["description"] || self.item.description
  end

  def owner_id
    collection.user_id
  end

  def activity_visible_to?(user)
    self.collection ? self.collection.visible_to?(user) : false
  end

  def comment_visible_to?(user)
    self.collection ? self.collection.visible_to?(user) : false
  end
end
