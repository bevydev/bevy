class SocialPermit < ActiveRecord::Base
  belongs_to :permissible, polymorphic: true
  belongs_to :admittable, polymorphic: true
  include PublicActivity::Model
  tracked except: :destroy,owner: ->(controller, model) { controller && controller.current_user }

  validates :permissible, presence: true
  validates :admittable, presence: true
  validates :permissible_id, uniqueness: { scope: [:admittable_id, :admittable_type, :permissible_type] }

  #use denied = false only for explicit prohibitons, never for simple revoking permission

  def self.publicly_permitted?(permissible)
    SocialPermit.public_permission_for(permissible).present?
  end

  def self.publicly_prohibited?(permissible)
    !SocialPermit.publicly_permitted?(permissible)
  end

  def self.permit_publicly(permissible)
    deny_permit = SocialPermit.get_permit(permissible, permissible.owner.public_circle, true)
    if deny_permit.present?
      deny_permit.update_attribute(:denied, false)
    else
      SocialPermit.create_permit(permissible, permissible.owner.public_circle) unless SocialPermit.publicly_permitted?(permissible)
    end
  end

  def self.prohibit_publicly(permissible)
    permission = SocialPermit.public_permission_for(permissible)
    permission.update_attribute(:denied, true) unless permission.blank?
  end


  def self.permit_type_for(arguments = {})
    assert_correct_arguments(arguments)

    if arguments[:to].is_a?(AdmittableGroup)
      return :prohibited if SocialPermit.prohibited?(what: arguments[:what], to: arguments[:to])
      return :public if SocialPermit.publicly_permitted?(arguments[:what])
      return :permitted if SocialPermit.permitted?(what: arguments[:what], to: arguments[:to])
      return :none
    else
      return SocialPermit.single_admittable_permit_type(arguments)
    end
  end

  def self.prohibit(arguments = {})
    assert_correct_arguments(arguments)
    return if arguments[:what].owner == arguments[:to]

    permission = SocialPermit.permission(arguments)
    if permission.present?
      permission.update_attribute(:denied, true)
    else
      SocialPermit.create_permit(arguments[:what], arguments[:to], true)
    end
  end

  def self.permit(arguments = {})
    assert_correct_arguments(arguments)
    return if arguments[:what].owner == arguments[:to]

    prohibition = SocialPermit.prohibition(arguments)
    if prohibition.present?
      prohibition.update_attribute(:denied, false)
    else
      SocialPermit.create_permit(arguments[:what], arguments[:to])
    end
  end

  def activity_visible_to?(user)
    self.permissible ? self.permissible.visible_to?(user) : false
  end

  private

  def self.get_permit(permissible, admittable, denied = false)
    SocialPermit.where(permissible_id: permissible.id,
                       admittable_id: admittable.id,
                       permissible_type: permissible.class.name,
                       admittable_type: admittable.class.name,
                       denied: denied).take
  end

  def self.create_permit(permissible, admittable, denied = false)
    SocialPermit.create(permissible_id: permissible.id,
                       admittable_id: admittable.id,
                       permissible_type: permissible.class.name,
                       admittable_type: admittable.class.name,
                       denied: denied)
  end

  def self.public_permission_for(permissible)
    SocialPermit.get_permit(permissible, permissible.owner.public_circle)
  end

  def self.permitted?(arguments = {})
    assert_correct_arguments(arguments)
    SocialPermit.permission(arguments).present?
  end

  def self.prohibited?(arguments = {})
    assert_correct_arguments(arguments)
    SocialPermit.prohibition(arguments).present?
  end

  def self.permission(arguments = {})
    assert_correct_arguments(arguments)
    SocialPermit.get_permit(arguments[:what], arguments[:to])
  end

  def self.prohibition(arguments = {})
    assert_correct_arguments(arguments)
    SocialPermit.get_permit(arguments[:what], arguments[:to], true)
  end

  def self.assert_correct_arguments(arguments = {})
    arguments.assert_valid_keys(:what, :to)
    raise ArgumentError.new unless arguments[:what].is_a?(Permissible) && arguments[:to].is_a?(Admittable)
  end

  #when checking permission for user we should consider his circle permissions unless user permit is specified
  def self.single_admittable_permit_type(arguments = {})
    assert_correct_arguments(arguments)
    raise ArgumentError.new if arguments[:to].is_a?(AdmittableGroup)

    return :prohibited if SocialPermit.prohibited?(what: arguments[:what], to: arguments[:to])
    return :permitted if SocialPermit.permitted?(what: arguments[:what], to: arguments[:to])

    admittable_groups = AdmittableGroup.all_groups_for(what: arguments[:what], to: arguments[:to])

    if admittable_groups.present?
      responses = []

      #if any AdmittableGroup which user belongs to has prohibited access, it prohibits him too from viewing permissible
      admittable_groups.each do |group|
        r = SocialPermit.permit_type_for(what: arguments[:what], to: group)
        if r == :prohibited
          return r
        else
          responses << r
        end
      end

      return :permitted if responses.any? { |permit| permit == :permitted }
    end

    return :public if SocialPermit.publicly_permitted?(arguments[:what])
    return :none
  end
end
