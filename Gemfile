source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.4'

gem 'devise'

gem 'sprockets-rails', '2.1.3'

gem 'jquery-scrollto-rails'

gem 'jquery-turbolinks'

gem 'fancybox-rails', :github => 'greinacker/fancybox-rails', :branch => 'rails4'

gem 'jquery-ui-rails'

gem 'elasticsearch-model'
gem 'elasticsearch-rails'
gem 'bonsai-elasticsearch-rails'
gem 'searchkick'
gem 'activeadmin', github: 'activeadmin'

gem 'public_activity'

# Use sqlite3 as the database for Active Record
group :development do
  gem 'sqlite3'
end

group :production do
  gem 'pg'
  gem 'rails_12factor'
end

# Use SCSS for stylesheets
gem 'sass-rails', '4.0.3'
# Use Autoprefixer to add browser vendor prefixes automatically
gem 'autoprefixer-rails'
# Use Bootstrap-SASS for Bootstrap framework support
gem 'bootstrap-sass', '~> 3.2.0'

# Use Cloudinary for flexible file upload
gem 'cloudinary'

# Use CarrierWave for flexible file upload
gem 'carrierwave'

gem 'ffi', '~> 1.9.5'

# Use Font Awesome toolkit for scalable vector icons
gem 'font-awesome-sass'

# Use MarkItUp for textarea on steroids
gem "markitup-rails"

gem 'mustache-js-rails'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '4.0.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '0.4.0',          group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring',        group: :development

#Windows timezone fix
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw]

# MongoDB and Mongoid things
gem "mongoid", "~> 4.0.0"
gem "bson_ext"
gem "tenacity"

gem 'simplecov', group: [:development, :test]

gem 'jquery-validation-rails'

gem "factory_girl_rails", "~> 4.0"

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
