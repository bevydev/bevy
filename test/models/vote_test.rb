require 'test_helper'

class VoteTest < ActiveSupport::TestCase

  test "should not save vote without user" do
  end

  test "should not save vote without votable" do
  end

  test "should not save vote without value" do
  end

  test "should not save more than one vote per user" do
  end

end
