require 'test_helper'

class ElementTest < ActiveSupport::TestCase
  def setup
    @element = Element.new
    @element.item_id = items(:one).id
    @element.collection_id = collections(:one).id
  end

  test "should be able to add connection between existing collection and item" do
    assert @element.save
    assert @element.destroy
  end
end
