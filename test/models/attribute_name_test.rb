require 'test_helper'

class AttributeNameTest < ActiveSupport::TestCase
  test "shouldn't be able to add two identical names" do
    attr_name = AttributeName.new
    attr_name.name = attribute_names(:one).name
    assert_not attr_name.save
  end
end
