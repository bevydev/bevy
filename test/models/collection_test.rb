require 'test_helper'


class CollectionTest < ActiveSupport::TestCase
  test "should not save collection without owner" do
    collection = Collection.new
    collection.name = "Music"
    collection.description = "My favorite cds"

    assert_not collection.save
  end

  test "should not save collection without name" do
    collection = Collection.new
    collection.user_id = 1
    collection.description = "My favorite cds"

    assert_not collection.save
  end

  test "should have unique name for one user" do
    first = Collection.find_by_user_id(User.first.id)

    second = first.dup
    second.description = "My most hated cds"

    assert_not second.save
  end

  test "should allow different users to use the same collection name" do
    first = Collection.find(3)

    second = first.dup
    second.user_id = User.second.id

    assert second.save
  end
end
