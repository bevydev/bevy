require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test "should not save user without name" do
    user = User.new(username: " ", email: "user@example.com", password: "foobar123", password_confirmation: "foobar123")
    assert_not user.save
  end

  test "should not save user without email" do
    user = User.new(username: "Example User", email: " ", password: "foobar123", password_confirmation: "foobar123")
    assert_not user.save
  end

  test "should not save user with invalid email" do
    user = User.new(username: "Example User", email: " ", password: "foobar123", password_confirmation: "foobar123")
    addresses = %w[user@foo,com user_at_foo.org example.user@foo.]
    addresses.each do |invalid_address|
      user.email = invalid_address
      assert_not user.save
    end
  end

  test "should save user with valid email" do
    user = User.new(username: "Example User", email: " ", password: "foobar123", password_confirmation: "foobar123")
    addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
    addresses.each do |valid_address|
      user.email = valid_address
      assert user.save
    end
  end

  test "should have unique username" do
    user = User.new(username: "Example User1", email: "user@example.com", password: "foobar123", password_confirmation: "foobar123")
    assert user.save
    user = User.new(username: "Example User1", email: "user2@example.com", password: "foobar123", password_confirmation: "foobar123")
    assert_not user.save
  end

  test "should have unique email" do
    user = User.new(username: "Example User", email: "user1@example.com", password: "foobar123", password_confirmation: "foobar123")
    assert user.save
    user = User.new(username: "Example User", email: "user2@example.com", password: "foobar123", password_confirmation: "foobar123")
    assert_not user.save
  end

  test "should not save user without password" do
    user = User.new(username: "Example User", email: "user@example.com", password: " ", password_confirmation: " ")
    assert_not user.save
  end

  test "should not save user when password does not match confirmation" do
    user = User.new(username: "Example User", email: "user@example.com", password: "foobar123", password_confirmation: "foobar124")
    assert_not user.save
  end

  test "should not save user with a password that is too short" do
    user = User.new(username: "Example User", email: "user@example.com", password: "foobar", password_confirmation: "foobar")
    assert_not user.save
  end
end
