require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods

  test "should not save comment without message" do
    assert_not build(:comment, message: nil).save
  end

  test "should not save comment without owner" do
    assert_not build(:comment, user_id: nil).save
  end

  test "should not save comment without commentable id" do
    assert_not build(:comment, commentable_id: nil).save
  end

  test "should not save comment without commentable type" do
    assert_not build(:comment, commentable_type: nil).save
  end

  test "should destroy item dependent comments" do
    item = create(:item)
    create(:comment, commentable_id: item.id)
    item.destroy
    assert_nil Comment.find_by(commentable_id: item.id)
  end

  test "should destroy element dependent comments" do
    element = create(:element)
    create(:comment, commentable_id: element.id, commentable_type: "Element")
    element.destroy
    assert_nil Comment.find_by(commentable_id: element.id)
  end

#   TODO: Comment should not have more than one parent

end
