FactoryGirl.define do

  factory :comment do
    commentable_id 1
    commentable_type "Item"
    user_id 1
    message "Message"
    rate 0
  end

  factory :user do
    username "Test user"
    email "user@test.com"
    password "password"
  end

  factory :item do
    user
    name "Test item"
    description "Lorem ipsum dolor sit amet"
    image "Image path"
  end

  factory :element do
    collection_id 1
    item
    description "Lorem ipsum dolor sit amet"
    image "Image path"
  end

end
