require 'test_helper'

class ElementsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    sign_in User.first
    @collection = Collection.find_by_user_id(User.first.id)
    @item = Item.find(7)
    @item_in_collection = @collection.items.first
    @element_in_collection = Element.where("item_id = ? AND collection_id = ?",
                                           @item_in_collection.id, @collection.id).first
  end

  test "should get #new with item not in collection as parameter" do
    get :new, collection_id: @collection, item_id: @item.id
    assert_response :success
  end

  test "should get #show with item already in collection as parameter" do
    get :new, collection_id: @collection, item_id: @item_in_collection.id
    assert_redirected_to collection_element_path(@collection, @element_in_collection)
  end

  test "should show element" do
    get :show, collection_id: @collection, id: @element_in_collection
    assert_response :success
  end

  test "should destroy item" do
    assert_difference('Element.count', -1) do
      delete :destroy, collection_id: @collection, id: @element_in_collection.id
    end

    assert_redirected_to collection_path(@collection)
  end

  test "user not signed in should be redirected to sign_in page" do
    sign_out User.first
    get :show, :collection_id => @collection, id: @element_in_collection.id
    assert_redirected_to new_user_session_path
  end


  test "should redirect to collection page when asked to show non-existing element" do
    get :show, id: 10000, collection_id: @collection
    assert_redirected_to collection_path(@collection)
  end
end