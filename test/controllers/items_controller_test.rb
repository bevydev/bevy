require 'test_helper'

class ItemsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    sign_in User.first
    @item = Item.first
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create item" do
    assert_difference('Item.count') do
      post :create, item: {name: "Totally new item"}
    end

    assert_redirected_to item_path(assigns(:item))
  end

  test "should show item" do
    get :show, id: @item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @item
    assert_response :success
  end

  test "should update item" do
    patch :update, id: @item, item: {name: "Totally new name"}
    assert_redirected_to item_path
  end

  test "should destroy item" do
    assert_difference('Item.count', -1) do
      delete :destroy, id: @item
    end

    assert_redirected_to items_path
  end

  test "unauthenticated user shouldn't be able to add, edit nor delete an item" do
    sign_out User.first
    get :new
    assert_redirected_to new_user_session_path
    get :edit, id: @item
    assert_redirected_to new_user_session_path
    delete :destroy, id: @item
    assert_redirected_to new_user_session_path
  end
end
