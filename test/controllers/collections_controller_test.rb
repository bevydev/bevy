require 'test_helper'

class CollectionsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @collection = Collection.find_by_user_id(User.first.id)
    sign_in User.first
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:collections)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create collection" do
    assert_difference('Collection.count') do
      post :create, collection: { user_id: User.first.id, name: "Totally new collection",
                                  description: "ONLY NOW: With totally new description!" }
    end

    assert_redirected_to collection_path(assigns(:collection))
  end

  test "should get edit" do
    get :edit, id: @collection
    assert_response :success
  end

  test "should update collection" do
    patch :update, id: @collection, collection: { name: "Brand new collection name" }
    assert_redirected_to collection_path(assigns(:collection))
  end

  test "should destroy collection" do
    assert_difference('Collection.count', -1) do
      delete :destroy, id: @collection
    end

    assert_redirected_to collections_path
  end

  test "collection destroy should trigger deletion of all corresponding elements" do
    assert_difference('Element.count', -@collection.elements.length) do
      delete :destroy, id: @collection
    end
  end

  test "user not signed in should be redirected to sign_in page" do
    sign_out User.first
    get :index
    assert_redirected_to new_user_session_path
  end

  test "user shouldn't be able to view collection of another user" do
    get :show, id: Collection.find_by_user_id(users(:two).id)
    assert_redirected_to collections_path
  end

  test "should redirect to collections page when asked to show non-existing collection" do
    get :show, id: Collection.new(id: 10000, name: "I don't exist")
    assert_redirected_to collections_path
  end

end
