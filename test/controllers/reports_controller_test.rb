require 'test_helper'

class ReportsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @created = Report.create(content: "Old report", title: "Old", user: User.first, issue_type: "Old")
    sign_in User.first
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:reports)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create report" do
    assert_difference('Report.count') do
      post :create, report: { content: "Test report", title: "Test", user: User.first, issue_type: "Testing" }
    end

    assert_redirected_to root_path
  end

  test "should show report" do
    get :show, id: @created
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @created
    assert_response :success
  end

  test "should update report" do
    patch :update, id: @created, report: { content: "Test report", title: "Test", issue_type: "Testing" }
    assert_redirected_to report_path(assigns(:report))
  end

  test "should destroy report" do
    assert_difference('Report.count', -1) do
      delete :destroy, id: @created
    end

    assert_redirected_to reports_path
  end
end
