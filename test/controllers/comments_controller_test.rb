require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @comment = comments(:one)
    sign_in User.first
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:comments)
  end

  test "should get new" do
    get :new, item_id: Item.first
    assert_response :success
  end

  test "should create comment" do
    assert_difference('Comment.count') do
      post :create, comment: { message: "Fine item!", commentable: Item.first }, item_id: Item.first
    end

    assert_redirected_to item_path(Item.first)
  end

  test "should show comment" do
    get :show, id: @comment, item_id: Item.first
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @comment, item_id: Item.first
    assert_response :success
  end

  test "should update comment" do
    patch :update, id: @comment, comment: { message: "new content!" }, item_id: Item.first
    assert_redirected_to item_path(Item.first)
  end

  test "should destroy comment" do
    assert_difference('Comment.count', -1) do
      delete :destroy, id: @comment
    end
  end
end
